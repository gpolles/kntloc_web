/**
 * @author Eberhard Graether / http://egraether.com/
 * @author Mark Lundin 	/ http://mark-lundin.com
 * @author Simone Manini / http://daron1337.github.io
 * @author Luca Antiga 	/ http://lantiga.github.io
 */

THREE.TrackballControls = function ( object, domElement ) {

	var _this = this;
	var STATE = { NONE: - 1, ROTATE: 0, ZOOM: 1, PAN: 2, TOUCH_ROTATE: 3, TOUCH_ZOOM_PAN: 4 };

	this.object = object;
	this.domElement = ( domElement !== undefined ) ? domElement : document;

	// API

	this.enabled = true;

	this.screen = { left: 0, top: 0, width: 0, height: 0 };

	this.rotateSpeed = 1.0;
	this.zoomSpeed = 1.2;
	this.panSpeed = 0.3;

	this.noRotate = false;
	this.noZoom = false;
	this.noPan = false;

	this.staticMoving = false;
	this.dynamicDampingFactor = 0.2;

	this.minDistance = 0;
	this.maxDistance = Infinity;

	this.keys = [ 65 /*A*/, 83 /*S*/, 68 /*D*/ ];

	// internals

	this.target = new THREE.Vector3();

	var EPS = 0.000001;

	var lastPosition = new THREE.Vector3();

	var _state = STATE.NONE,
	_prevState = STATE.NONE,

	_eye = new THREE.Vector3(),

	_movePrev = new THREE.Vector2(),
	_moveCurr = new THREE.Vector2(),

	_lastAxis = new THREE.Vector3(),
	_lastAngle = 0,

	_zoomStart = new THREE.Vector2(),
	_zoomEnd = new THREE.Vector2(),

	_touchZoomDistanceStart = 0,
	_touchZoomDistanceEnd = 0,

	_panStart = new THREE.Vector2(),
	_panEnd = new THREE.Vector2();

	// for reset

	this.target0 = this.target.clone();
	this.position0 = this.object.position.clone();
	this.up0 = this.object.up.clone();

	// events

	var changeEvent = { type: 'change' };
	var startEvent = { type: 'start' };
	var endEvent = { type: 'end' };


	// methods

	this.handleResize = function () {

		if ( this.domElement === document ) {

			this.screen.left = 0;
			this.screen.top = 0;
			this.screen.width = window.innerWidth;
			this.screen.height = window.innerHeight;

		} else {

			var box = this.domElement.getBoundingClientRect();
			// adjustments come from similar code in the jquery offset() function
			var d = this.domElement.ownerDocument.documentElement;
			this.screen.left = box.left + window.pageXOffset - d.clientLeft;
			this.screen.top = box.top + window.pageYOffset - d.clientTop;
			this.screen.width = box.width;
			this.screen.height = box.height;

		}

	};

	this.handleEvent = function ( event ) {

		if ( typeof this[ event.type ] == 'function' ) {

			this[ event.type ]( event );

		}

	};

	var getMouseOnScreen = ( function () {

		var vector = new THREE.Vector2();

		return function getMouseOnScreen( pageX, pageY ) {

			vector.set(
				( pageX - _this.screen.left ) / _this.screen.width,
				( pageY - _this.screen.top ) / _this.screen.height
			);

			return vector;

		};

	}() );

	var getMouseOnCircle = ( function () {

		var vector = new THREE.Vector2();

		return function getMouseOnCircle( pageX, pageY ) {

			vector.set(
				( ( pageX - _this.screen.width * 0.5 - _this.screen.left ) / ( _this.screen.width * 0.5 ) ),
				( ( _this.screen.height + 2 * ( _this.screen.top - pageY ) ) / _this.screen.width ) // screen.width intentional
			);

			return vector;

		};

	}() );

	this.rotateCamera = ( function() {

		var axis = new THREE.Vector3(),
			quaternion = new THREE.Quaternion(),
			eyeDirection = new THREE.Vector3(),
			objectUpDirection = new THREE.Vector3(),
			objectSidewaysDirection = new THREE.Vector3(),
			moveDirection = new THREE.Vector3(),
			angle;

		return function rotateCamera() {

			moveDirection.set( _moveCurr.x - _movePrev.x, _moveCurr.y - _movePrev.y, 0 );
			angle = moveDirection.length();

			if ( angle ) {

				_eye.copy( _this.object.position ).sub( _this.target );

				eyeDirection.copy( _eye ).normalize();
				objectUpDirection.copy( _this.object.up ).normalize();
				objectSidewaysDirection.crossVectors( objectUpDirection, eyeDirection ).normalize();

				objectUpDirection.setLength( _moveCurr.y - _movePrev.y );
				objectSidewaysDirection.setLength( _moveCurr.x - _movePrev.x );

				moveDirection.copy( objectUpDirection.add( objectSidewaysDirection ) );

				axis.crossVectors( moveDirection, _eye ).normalize();

				angle *= _this.rotateSpeed;
				quaternion.setFromAxisAngle( axis, angle );

				_eye.applyQuaternion( quaternion );
				_this.object.up.applyQuaternion( quaternion );

				_lastAxis.copy( axis );
				_lastAngle = angle;

			} else if ( ! _this.staticMoving && _lastAngle ) {

				_lastAngle *= Math.sqrt( 1.0 - _this.dynamicDampingFactor );
				_eye.copy( _this.object.position ).sub( _this.target );
				quaternion.setFromAxisAngle( _lastAxis, _lastAngle );
				_eye.applyQuaternion( quaternion );
				_this.object.up.applyQuaternion( quaternion );

			}

			_movePrev.copy( _moveCurr );

		};

	}() );


	this.zoomCamera = function () {

		var factor;

		if ( _state === STATE.TOUCH_ZOOM_PAN ) {

			factor = _touchZoomDistanceStart / _touchZoomDistanceEnd;
			_touchZoomDistanceStart = _touchZoomDistanceEnd;
			_eye.multiplyScalar( factor );

		} else {

			factor = 1.0 + ( _zoomEnd.y - _zoomStart.y ) * _this.zoomSpeed;

			if ( factor !== 1.0 && factor > 0.0 ) {

				_eye.multiplyScalar( factor );

			}

			if ( _this.staticMoving ) {

				_zoomStart.copy( _zoomEnd );

			} else {

				_zoomStart.y += ( _zoomEnd.y - _zoomStart.y ) * this.dynamicDampingFactor;

			}

		}

	};

	this.panCamera = ( function() {

		var mouseChange = new THREE.Vector2(),
			objectUp = new THREE.Vector3(),
			pan = new THREE.Vector3();

		return function panCamera() {

			mouseChange.copy( _panEnd ).sub( _panStart );

			if ( mouseChange.lengthSq() ) {

				mouseChange.multiplyScalar( _eye.length() * _this.panSpeed );

				pan.copy( _eye ).cross( _this.object.up ).setLength( mouseChange.x );
				pan.add( objectUp.copy( _this.object.up ).setLength( mouseChange.y ) );

				_this.object.position.add( pan );
				_this.target.add( pan );

				if ( _this.staticMoving ) {

					_panStart.copy( _panEnd );

				} else {

					_panStart.add( mouseChange.subVectors( _panEnd, _panStart ).multiplyScalar( _this.dynamicDampingFactor ) );

				}

			}

		};

	}() );

	this.checkDistances = function () {

		if ( ! _this.noZoom || ! _this.noPan ) {

			if ( _eye.lengthSq() > _this.maxDistance * _this.maxDistance ) {

				_this.object.position.addVectors( _this.target, _eye.setLength( _this.maxDistance ) );
				_zoomStart.copy( _zoomEnd );

			}

			if ( _eye.lengthSq() < _this.minDistance * _this.minDistance ) {

				_this.object.position.addVectors( _this.target, _eye.setLength( _this.minDistance ) );
				_zoomStart.copy( _zoomEnd );

			}

		}

	};

	this.update = function () {

		_eye.subVectors( _this.object.position, _this.target );

		if ( ! _this.noRotate ) {

			_this.rotateCamera();

		}

		if ( ! _this.noZoom ) {

			_this.zoomCamera();

		}

		if ( ! _this.noPan ) {

			_this.panCamera();

		}

		_this.object.position.addVectors( _this.target, _eye );

		_this.checkDistances();

		_this.object.lookAt( _this.target );

		if ( lastPosition.distanceToSquared( _this.object.position ) > EPS ) {

			_this.dispatchEvent( changeEvent );

			lastPosition.copy( _this.object.position );

		}

	};

	this.reset = function () {

		_state = STATE.NONE;
		_prevState = STATE.NONE;

		_this.target.copy( _this.target0 );
		_this.object.position.copy( _this.position0 );
		_this.object.up.copy( _this.up0 );

		_eye.subVectors( _this.object.position, _this.target );

		_this.object.lookAt( _this.target );

		_this.dispatchEvent( changeEvent );

		lastPosition.copy( _this.object.position );

	};

	// listeners

	function keydown( event ) {

		if ( _this.enabled === false ) return;

		window.removeEventListener( 'keydown', keydown );

		_prevState = _state;

		if ( _state !== STATE.NONE ) {

			return;

		} else if ( event.keyCode === _this.keys[ STATE.ROTATE ] && ! _this.noRotate ) {

			_state = STATE.ROTATE;

		} else if ( event.keyCode === _this.keys[ STATE.ZOOM ] && ! _this.noZoom ) {

			_state = STATE.ZOOM;

		} else if ( event.keyCode === _this.keys[ STATE.PAN ] && ! _this.noPan ) {

			_state = STATE.PAN;

		}

	}

	function keyup( event ) {

		if ( _this.enabled === false ) return;

		_state = _prevState;

		window.addEventListener( 'keydown', keydown, false );

	}

	function mousedown( event ) {

		if ( _this.enabled === false ) return;

		event.preventDefault();
		event.stopPropagation();

		if ( _state === STATE.NONE ) {

			_state = event.button;

		}

		if ( _state === STATE.ROTATE && ! _this.noRotate ) {

			_moveCurr.copy( getMouseOnCircle( event.pageX, event.pageY ) );
			_movePrev.copy( _moveCurr );

		} else if ( _state === STATE.ZOOM && ! _this.noZoom ) {

			_zoomStart.copy( getMouseOnScreen( event.pageX, event.pageY ) );
			_zoomEnd.copy( _zoomStart );

		} else if ( _state === STATE.PAN && ! _this.noPan ) {

			_panStart.copy( getMouseOnScreen( event.pageX, event.pageY ) );
			_panEnd.copy( _panStart );

		}

		document.addEventListener( 'mousemove', mousemove, false );
		document.addEventListener( 'mouseup', mouseup, false );

		_this.dispatchEvent( startEvent );

	}

	function mousemove( event ) {

		if ( _this.enabled === false ) return;

		event.preventDefault();
		event.stopPropagation();

		if ( _state === STATE.ROTATE && ! _this.noRotate ) {

			_movePrev.copy( _moveCurr );
			_moveCurr.copy( getMouseOnCircle( event.pageX, event.pageY ) );

		} else if ( _state === STATE.ZOOM && ! _this.noZoom ) {

			_zoomEnd.copy( getMouseOnScreen( event.pageX, event.pageY ) );

		} else if ( _state === STATE.PAN && ! _this.noPan ) {

			_panEnd.copy( getMouseOnScreen( event.pageX, event.pageY ) );

		}

	}

	function mouseup( event ) {

		if ( _this.enabled === false ) return;

		event.preventDefault();
		event.stopPropagation();

		_state = STATE.NONE;

		document.removeEventListener( 'mousemove', mousemove );
		document.removeEventListener( 'mouseup', mouseup );
		_this.dispatchEvent( endEvent );

	}

	function mousewheel( event ) {

		if ( _this.enabled === false ) return;

		event.preventDefault();
		event.stopPropagation();

		switch ( event.deltaMode ) {

			case 2:
				// Zoom in pages
				_zoomStart.y -= event.deltaY * 0.025;
				break;

			case 1:
				// Zoom in lines
				_zoomStart.y -= event.deltaY * 0.01;
				break;

			default:
				// undefined, 0, assume pixels
				_zoomStart.y -= event.deltaY * 0.00025;
				break;

		}

		_this.dispatchEvent( startEvent );
		_this.dispatchEvent( endEvent );

	}

	function touchstart( event ) {

		if ( _this.enabled === false ) return;

		switch ( event.touches.length ) {

			case 1:
				_state = STATE.TOUCH_ROTATE;
				_moveCurr.copy( getMouseOnCircle( event.touches[ 0 ].pageX, event.touches[ 0 ].pageY ) );
				_movePrev.copy( _moveCurr );
				break;

			default: // 2 or more
				_state = STATE.TOUCH_ZOOM_PAN;
				var dx = event.touches[ 0 ].pageX - event.touches[ 1 ].pageX;
				var dy = event.touches[ 0 ].pageY - event.touches[ 1 ].pageY;
				_touchZoomDistanceEnd = _touchZoomDistanceStart = Math.sqrt( dx * dx + dy * dy );

				var x = ( event.touches[ 0 ].pageX + event.touches[ 1 ].pageX ) / 2;
				var y = ( event.touches[ 0 ].pageY + event.touches[ 1 ].pageY ) / 2;
				_panStart.copy( getMouseOnScreen( x, y ) );
				_panEnd.copy( _panStart );
				break;

		}

		_this.dispatchEvent( startEvent );

	}

	function touchmove( event ) {

		if ( _this.enabled === false ) return;

		event.preventDefault();
		event.stopPropagation();

		switch ( event.touches.length ) {

			case 1:
				_movePrev.copy( _moveCurr );
				_moveCurr.copy( getMouseOnCircle( event.touches[ 0 ].pageX, event.touches[ 0 ].pageY ) );
				break;

			default: // 2 or more
				var dx = event.touches[ 0 ].pageX - event.touches[ 1 ].pageX;
				var dy = event.touches[ 0 ].pageY - event.touches[ 1 ].pageY;
				_touchZoomDistanceEnd = Math.sqrt( dx * dx + dy * dy );

				var x = ( event.touches[ 0 ].pageX + event.touches[ 1 ].pageX ) / 2;
				var y = ( event.touches[ 0 ].pageY + event.touches[ 1 ].pageY ) / 2;
				_panEnd.copy( getMouseOnScreen( x, y ) );
				break;

		}

	}

	function touchend( event ) {

		if ( _this.enabled === false ) return;

		switch ( event.touches.length ) {

			case 0:
				_state = STATE.NONE;
				break;

			case 1:
				_state = STATE.TOUCH_ROTATE;
				_moveCurr.copy( getMouseOnCircle( event.touches[ 0 ].pageX, event.touches[ 0 ].pageY ) );
				_movePrev.copy( _moveCurr );
				break;

		}

		_this.dispatchEvent( endEvent );

	}

	function contextmenu( event ) {

		if ( _this.enabled === false ) return;

		event.preventDefault();

	}

	this.dispose = function() {

		this.domElement.removeEventListener( 'contextmenu', contextmenu, false );
		this.domElement.removeEventListener( 'mousedown', mousedown, false );
		this.domElement.removeEventListener( 'wheel', mousewheel, false );

		this.domElement.removeEventListener( 'touchstart', touchstart, false );
		this.domElement.removeEventListener( 'touchend', touchend, false );
		this.domElement.removeEventListener( 'touchmove', touchmove, false );

		document.removeEventListener( 'mousemove', mousemove, false );
		document.removeEventListener( 'mouseup', mouseup, false );

		window.removeEventListener( 'keydown', keydown, false );
		window.removeEventListener( 'keyup', keyup, false );

	};

	this.domElement.addEventListener( 'contextmenu', contextmenu, false );
	this.domElement.addEventListener( 'mousedown', mousedown, false );
	this.domElement.addEventListener( 'wheel', mousewheel, false );

	this.domElement.addEventListener( 'touchstart', touchstart, false );
	this.domElement.addEventListener( 'touchend', touchend, false );
	this.domElement.addEventListener( 'touchmove', touchmove, false );

	window.addEventListener( 'keydown', keydown, false );
	window.addEventListener( 'keyup', keyup, false );

	this.handleResize();

	// force an update at start
	this.update();

};

THREE.TrackballControls.prototype = Object.create( THREE.EventDispatcher.prototype );
THREE.TrackballControls.prototype.constructor = THREE.TrackballControls;
// deepcopy an object
function deepcopy(obj){
  return JSON.parse(JSON.stringify(obj));
}

// python style range
function range(start, end, increment) {
    var array = [];
    var current = start;

    increment = increment || 1;
    if (increment > 0) {
        while (current < end) {
            array.push(current);
            current += increment;
        }
    } else {
        while (current > end) {
            array.push(current);
            current += increment;
        }
    }
    return array;
}

// close-to-unique-uids
function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
}


// check proximity of 3d points
function allClose(a1, a2, tol=0.0001){
  return a1.length==a2.length && a1.every(function(v,i) { return Math.abs(v - a2[i]) < tol})
}


// check if coordinates are a ring
function isRing(coordinates){
  return allClose(coordinates[0], coordinates[coordinates.length - 1]);
}


// utilities for overlapping ranges
function RangeEvent(pos, id, etype){
  this.x = pos;
  this.id = id;
  this.start = ( etype === 'start' );
  this.stop = !this.start; 
}

function stackUniqueRanges(ranges, n) { 
  // superimpose in order of importance input ranges

  // populate events
  var events = [];
  for (var i = 0; i < ranges.length; i++) {
  
    var start = ranges[i][0];
    var stop = ranges[i][1];
    events.push( new RangeEvent(start, i, 'start') );
    events.push( new RangeEvent(stop, i, 'stop') );
  
    if ( start > stop ){
    
      events.push( new RangeEvent(0, i, 'start') );
      events.push( new RangeEvent(n, i, 'stop') );

    }
    
  }

  // sort events by position and then by id
  // smaller id are prioritized
  // start goes before end
  events.sort(function(a, b) {
  
    return a.x - b.x || a.id - b.id || (a.start ? -1 : 1);
    
  });


  var currentStatus = Infinity;
  var rangeStart = false;
  var newRanges = [];
  var statusStack = [];


  console.log(currentStatus, rangeStart, statusStack);

  for (var i = 0; i < events.length; i++) {
    
    var e = events[i];
    if( e.start ) {

      statusStack.push( e.id );
      statusStack.sort();
      if ( e.id < currentStatus ) {

        if (rangeStart !== false && rangeStart !== e.x ) {

          newRanges.push( [currentStatus, rangeStart, e.x] );

        }

        currentStatus = e.id;
        rangeStart = e.x;

      } 

    }
    if ( e.stop ){

      if ( e.id === currentStatus) {

        if ( rangeStart !== e.x )
          newRanges.push( [e.id, rangeStart, e.x] );
        statusStack.splice(0, 1);
        if (statusStack.length > 0){

          rangeStart = e.x;
          currentStatus = statusStack[0];
        
        } else {
        
          rangeStart = false;
          currentStatus = Infinity;
        
        }

      } else {

        var idx = statusStack.indexOf(e.id);
        statusStack.splice(idx, 1);

      }

    }

  }

  return newRanges;

}

function stackRanges(ids, ranges, n) { 
  // stack ranges

  // populate events
  var events = [];
  for (var i = 0; i < ranges.length; i++) {
  
    var start = ranges[i][0];
    var stop = ranges[i][1];
    events.push( new RangeEvent(start, ids[i], 'start') );
    events.push( new RangeEvent(stop, ids[i], 'stop') );
  
    if ( start > stop ){
    
      events.push( new RangeEvent(0, ids[i], 'start') );
      events.push( new RangeEvent(n, ids[i], 'stop') );

    }
    
  }

  // sort events by position and then by id
  // smaller id are prioritized
  // start goes before end
  events.sort(function(a, b) {
  
    return a.x - b.x || a.id - b.id || (a.start ? -1 : 1);
    
  });


  var rangeStart = false;
  var newRanges = [];
  var statusStack = [];

  for (var i = 0; i < events.length; i++) {
    var e = events[i];

    if( e.start ) {

      if ( rangeStart !== false && rangeStart !== e.x ) {

        newRanges.push( [statusStack.slice(), rangeStart, e.x] );

      }

      rangeStart = e.x;
      statusStack.push( e.id ); 

    }

    if ( e.stop ){

      if ( rangeStart !== false && rangeStart !== e.x )
        newRanges.push( [statusStack.slice(), rangeStart, e.x] );

      var idx = statusStack.indexOf(e.id);
      statusStack.splice(idx, 1);

      if ( statusStack.length > 0 ){

        rangeStart = e.x;
        
      } else {
      
        rangeStart = false;
      
      }

    }


    // console.log(i, 'pos', e.x, 'id', e.id, 'type', (e.start ? 'start' : 'stop'));
    // console.log('currentStatus', currentStatus, 'rangeStart', rangeStart, 'stack', statusStack);

  }

  return newRanges;

}

function parsePDB( text , onlyCA=false ) {

  var lines = text.split( '\n' );
  var chains = [];
  chains.chainNames = [];
  var last_chain = null;
  var curr_chain = [];
  curr_chain.coordinates = [];
  curr_chain.resSeqs = [];
  curr_chain.resNames = [];
  curr_chain.names = [];

  for ( var i = 0, l = lines.length; i < l; i ++ ) {

    if ( lines[ i ].substr( 0, 4 ) === 'ATOM' || lines[ i ].substr( 0, 6 ) === 'HETATM' ) {

      var name = lines[ i ].substring( 12, 16 ).trim();
      if (onlyCA && name !== "CA") continue;

      x = parseFloat( lines[ i ].substring( 30, 38 ) );
      y = parseFloat( lines[ i ].substring( 38, 46 ) );
      z = parseFloat( lines[ i ].substring( 46, 54 ) );

      var resSeq = parseInt( lines[ i ].substring( 22, 26 ) );
      var resName = lines[ i ].substring( 17, 20 ).trim();
      var chain = lines[ i ].substring( 21, 22 );

      if ( chain !== last_chain ){
        if (curr_chain && curr_chain.length > 0){
          chains.push( curr_chain );
          chains.chainNames.push( curr_chain.chainName );
        }
        curr_chain = [];
        curr_chain.chainName = chain; 
        curr_chain.coordinates = [];
        curr_chain.resSeqs = [];
        curr_chain.resNames = [];
        curr_chain.names = [];
        last_chain = chain;
      }

      curr_chain.push({
        name: name,
        resSeq: resSeq,
        resName: resName,
        pos: [x, y, z],
      });
      curr_chain.names.push(name);
      curr_chain.coordinates.push([x,y,z]);
      curr_chain.resSeqs.push(resSeq);
      curr_chain.resNames.push(resName);
    }
  }
  if (curr_chain && curr_chain.length > 0){
    chains.push( curr_chain );
    chains.chainNames.push( curr_chain.chainName );
  }
  return chains
}


function parseKNT(fileString){
  
  var lines = fileString.split( "\n" );
  var n_atoms = parseInt( lines[0] );
  
  if ( isNaN( n_atoms ) || n_atoms <= 0 || lines.length < n_atoms + 1 ) {
  
    alert( 'Cannot read NXYZ file. Malformed input at line 1.' );
    return false;
  
  }
  
  var coordinates = [];
  var frameStart = 0;

  while ( frameStart < lines.length ) {

    if ( lines[frameStart].trim() === "" ) {
      
      frameStart++;
      continue;
    
    }

    var n_atoms_2 = parseInt( lines[ frameStart ] );
    
    if ( n_atoms_2 !== n_atoms ) {
    
      alert( 'Cannot read NXYZ file. Malformed input at line ' + ( frameStart + 1 ) );
      return false;
    
    }
  
    var frame = [];

    for ( var i = frameStart; i < frameStart + n_atoms; ++i ) {  
    
      var data = lines[i+1].match(/\S+/g) || [];
    
      if ( data.length < 3 ){
    
        alert( 'Cannot read NXYZ file. Malformed input at line ' + ( i + 2 ) );
        return false;
    
      }
    
      var x = parseFloat( data[0] );
      var y = parseFloat( data[1] );
      var z = parseFloat( data[2] );
    
      if ( isNaN(x) || isNaN(y) || isNaN(z) ) {
    
        alert( 'Cannot read NXYZ file. Malformed input at line ' + ( i + 2 ) );
        return false;
    
      }
    
      frame.push( [ x, y, z ] );
    
    }  

    coordinates.push(frame);
    frameStart += n_atoms + 1;

  }

  return coordinates;

}
var KnotFileLoader = function(domfield, opt){

  var self = this;
  var opt = opt || {};

  self.domfield = domfield;
  self.fileExtension = null;
  self.maxFileSize = opt.maxFileSize || 80*1024*1024;
  self.data = null;
  self.dataType = null;
  
  self.beforestart = opt.beforestart || function() {};
  self.onerror = opt.onerror || function() {};
  self.onprogress = opt.onprogress || function() {};
  self.onload = opt.onload || function() {};

  self.startRead = function () {
      // obtain input element through DOM
    self.beforestart(self);
    var file = self.domfield.files[0];
    self.fileExtension = file.name.split('.').pop();
    if(file){
      if (file.size > self.maxFileSize){
        self.errorHandler("Sorry, due to the limited web resources the file you choose is too large to be processed interactively." +
                          "\nPlease select a smaller file or download and run the software locally from the download page.")
        return false;
      }
      self.getAsText(file);
    }
  }

  self.getAsText = function(readFile) {

    var reader = new FileReader();
    // Read file into memory as UTF-8
    reader.readAsText(readFile, "UTF-8");

    // Handle progress, success, and errors
    reader.onprogress = self.onprogress;
    reader.onload = self.loaded;
    reader.onerror = self.errorHandler;

  }

  self.errorHandler = function(evt) {
    self.error = evt;
    self.onerror(self);
  }


  self.loaded = function (evt) {

    // Obtain the read file data
    var fileContent = evt.target.result;
    // Handle UTF-16 file dump
    self.parseContent(fileContent);
  
  }

  self.parseContent = function (fileContent) {

    var coordinates = null;

    if ( self.fileExtension == 'pdb' ) {
    
      self.data = parsePDB(fileContent, true);
      if ( self.data.length === 0 ) {
      
        self.errorHandler('Error reading coordinates (does the PDB file contain CA atoms?)');
        return false;
      
      } 
      self.dataType = 'pdb';
    
    } else {
      
      self.data = parseKNT(fileContent);
      
      if ( !self.data ){
      
        self.errorHandler('Error reading NXYZ coordinates');
        return false;
      
      }

      self.dataType = 'knt';

    }

    self.onload(self);
      
  }

}

var AppStatus = function(){

  var self = this;

  self.all_coordinates = []; // all loaded coordinates
  self.chains = []; // structures holding additional information from pdb
  
  self.num_beads = 0; self.num_chains = 0; self.num_frames = 0;
  self.current_chain = 0; self.current_frame = 0; 

  self.trajectory_flag = false; self.pdb_flag = false; self.ring_polymer_flag = false;

  self.active_beads_range = false;

  self.knot_positions = [];

  self.apply_closure = [];

  self.runOptions = {

    full: false,
    topdown : false,
    stride: -1,

  }
  self.kymo = [ false ];

  this.updateInternals = function(){


    self.num_chains = self.chains.length;
    self.num_frames = self.num_chains ? 1 : self.all_coordinates.length;
    self.num_beads = self.all_coordinates.length > 0 ? self.all_coordinates[0].length : 0;
    
    self.current_frame = 0;
    self.current_chain = 0;

    self.trajectory_flag = self.num_frames > 1;
    self.pdb_flag = self.num_chains > 0;
    self.ring_polymer_flag = ( self.num_beads > 0 ) ? isRing( self.all_coordinates[0] ) : false;

    self.kymo = self.trajectory_flag ? [ false ] : new Array( self.num_chains );

    self.active_beads_range = [0, self.num_beads - 1];

    self.knot_positions = new Array( self.num_chains || self.num_frames );
    self.knot_positions.fill(false);

    self.knot_type = new Array( self.num_chains || self.num_frames );
    self.knot_type.fill(false);

    self.alexander_dets = new Array( self.num_chains || self.num_frames );
    self.alexander_dets.fill(false);

    self.apply_closure = new Array( self.num_chains || self.num_frames );
    self.apply_closure.fill(false);

  }

  self.setTrajectory = function(coordinates){
    if (coordinates === null){

      console.log('error on setTrajectory');
      return false;

    }
    self.chains = [];
    self.all_coordinates = coordinates;
    self.updateInternals();
  }

  self.getTrajectory = function(){
    
    if ( self.all_coordinates.length && self.isTrajectory() ){

      return self.all_coordinates;

    }

    return false;

  }

  self.setChains = function(chains){
    self.chains = chains;
    self.all_coordinates = [];
    for (var i = 0; i < self.chains.length; i++) {
      self.all_coordinates.push(self.chains[i].coordinates);
    }
    self.updateInternals();
  }

  self.getBeadPdbInfo = function( i ) {

    if ( self.pdb_flag )
      return self.chains[ self.current_chain ][ i ].resSeq;
  }

  self.setCurrentFrame = function( i ) {

    if ( i >= 0 || i < self.num_frames )
      self.current_frame = i;
    return self.current_frame;
  
  }

  self.setCurrentChain = function( i ) {

    if ( i >= 0 || i < self.num_chains )
      self.current_chain = i;

    self.num_beads = self.all_coordinates[ i ].length;
    self.active_beads_range = [0, self.num_beads - 1];
    return self.current_chain;
  
  }

  self.getCurrentChain = function() {

    return self.current_chain;
  
  }

  self.getCurrentChainObject = function() {

    return self.chains[ self.current_chain ];
  
  }

  self.closeRing = function( struct_id ) {

    if (! isRing( self.all_coordinates[ struct_id ] ) ) {

      self.apply_closure[ struct_id ] = true;
      self.all_coordinates[ struct_id ].push( self.all_coordinates[ struct_id ][ 0 ] );
      if ( struct_id == self.getCurrentIndex() ) {

        self.num_beads = self.all_coordinates[ struct_id ].length;
        self.active_beads_range = [0, self.num_beads-1];
        
      } 
      
    }
    
  }

  self.openRing = function( struct_id ) {

    if ( isRing( self.all_coordinates[ struct_id ] ) ) {
      
      self.apply_closure[ struct_id ] = false;
      self.all_coordinates[ struct_id ].pop();
      if ( struct_id == self.getCurrentIndex() ) {

        self.num_beads = self.all_coordinates[ struct_id ].length;
        self.active_beads_range = [0, self.num_beads-1];
        
      } 

    }

  }

  self.getCurrentStructure = function() {
    
    return self.all_coordinates[ self.getCurrentIndex() ];
  
  }

  self.setActiveBeadRange = function(x1, x2) {

    var x1 = x1 || 0;
    var x2 = (x2 === undefined) ? self.num_beads - 1 : x2;
    
    if ( x1 < 0 || x2 < 0 || x1 >= self.num_beads || x2 >= self.num_beads ){

      console.log('invalid range', x1, x2);
      return false;

    }

    self.active_beads_range = [x1, x2];

  }

  self.getActiveBeadRange = function() {

    return ( self.active_beads_range || [0, self.num_beads] );

  }

  self.setKnotRange = function(x1, x2, struct_id=0) {

    self.knot_positions[ struct_id ] = [parseInt(x1), parseInt(x2)];

  }
 
  self.getKnotRange = function(struct_id=false) {

    if ( struct_id === false ) {

      struct_id = self.getCurrentIndex();
      
    }

    return self.knot_positions[ struct_id ];

  } 

  self.getCurrentIndex = function(){

    if ( self.trajectory_flag ) 
      return self.current_frame;
    else 
      return self.current_chain;
  
  }

  self.isTrajectory = function () {
    
    return self.trajectory_flag;

  }

  self.isRing = function() {
    
    return isRing( self.all_coordinates[ self.getCurrentIndex() ] );
  
  }

  self.setRunOptions = function( opts ) {

    Object.assign(self.runOptions, opts);

  }

  self.getRunOptions = function( name ) {

    if ( name ){

      return self.runOptions[ name ];

    } 

    return self.runOptions;

  }


  self.setKnotInfo = function( data ) {

    if ( data.num_frames !== self.num_frames ) {
      console.log('data.num_frames !== self.num_frames', data.num_frames, self.num_frames);
      return false;
    }

    var r = data.results;

    for (var i = 0; i < self.num_frames; i++) {
      
      self.knot_positions[i] = [ r.pos[ i ][ 0 ], r.pos[ i ][ 1 ] ];
      self.knot_type[i] = r.ktype[ i ];

    }

  }

  self.hasPdbInfo = function() {

    return self.pdb_flag;

  }

}
var InterfaceControl = function() {

  var self = this;
  var slider = $("input#slider-range").slider({ id: "slider", min: 0, max: 0, range: true, value: [0, 0] });
  var sliderOn = "rgba(0, 0, 0, 0) linear-gradient(rgb(209, 209, 209) 0px, rgb(205, 205, 205) 100%) repeat-x";
  var sliderOff = "rgba(0,0,0,0)";

  self.deactivate = function() {
  
    // disable all action while computing
    $("button#select-file-btn").prop('disabled', true);
    $("#download-link").off("click");
    $('button#run-job-btn').prop('disabled', true);
    $(".toolbar-link").prop('disabled', true);
    $(".toolbar-link:not(.disabled)").addClass('disabled');
    $("input, select").prop('disabled', true);
    slider.slider("disable");
    $("#traj-next, #traj-prev").prop('disabled', true);

  }

  self.activate = function() {

    $("button#select-file-btn").prop('disabled', false);
    $('button#run-job-btn').prop('disabled', false);
    $(".toolbar-link").prop('disabled', false);
    $('.nav-link.disabled').removeClass("disabled");
    $("input, select").prop('disabled', false);
    slider.slider("enable");
    $("#traj-next, #traj-prev").prop('disabled', false);

  }

  self.updateControls = function(status, selector){

    // show controls
    if ( status.num_beads > 0 )
      $('div#control-window').removeClass("invisible");
    else
      $('div#control-window').addClass("invisible");

    // trajectory controls
    if ( selector === 'all' || selector === 'traj' ) {

      if ( status.num_frames > 1 )
        $('#trajectory-controls').show();
      else
        $('#trajectory-controls').hide();
      $('#traj-frame').html( 'Frame ' + ( status.current_frame + 1 ) + ' of ' + status.num_frames );

    } 

    // chain controls
    if ( selector === 'all' || selector === 'chain' ) {

      if ( status.num_chains > 1 ){
        
        var select = $('#chain-selector');
        select.empty(); // remove old options
        for (var i = 0; i < status.chains.length; i++) {
        
          select.append($("<option></option>").attr("value", i)
                                              .text(status.chains[i].chainName + ' (' + 
                                                    status.chains[i].length + ' points)'));
        
        }
        select.val(status.getCurrentChain());
        $('#chain-selector-form').show();

      } else {

        $('#chain-selector-form').hide();

      }

      if ( status.ring_polymer_flag ) {
        $('#ring-controls').hide();
      } else {
        $('#ring-controls').show();
      }

    }

    // bead range controls
    if ( selector === 'all' || selector === 'bead-range' ) {

      var beadRange = status.getActiveBeadRange();

      var x1 = Math.min(beadRange[ 0 ], beadRange[ 1 ]);
      var x2 = Math.max(beadRange[ 0 ], beadRange[ 1 ]);
      var invert = beadRange[ 0 ] > beadRange[ 1 ];


      $("#bead-interval-invert").prop('checked', invert);
      $("#bead-interval-start").attr("max", status.num_beads - 1);
      $("#bead-interval-end").attr("max", status.num_beads - 1);
      $('#bead-interval-start').val(x1);
      $('#bead-interval-end').val(x2);      
      slider.slider('setAttribute', 'max', status.num_beads -1);
      slider.slider('setValue', [x1, x2]);
      $('#bead-interval-label1').html((invert ? 'End' : 'Begin'));
      $('#bead-interval-label2').html((invert ? 'Begin' : 'End'));
      $("#slider .slider-selection").css("background", 
                                         invert ? sliderOff : sliderOn);  
      $("#slider .slider-track-low," +
        "#slider .slider-track-high").css("background", 
                                          invert ? sliderOn : sliderOff);

      if ( status.hasPdbInfo() ) {

        $('#bead-interval-pdb-info').show();
        var v1 = status.getCurrentChainObject()[ x1 ];
        var v2 = null;
        if ( x2 == status.num_beads - 1 && 
             status.apply_closure[status.getCurrentIndex()] ){
            v2 = status.getCurrentChainObject()[ 0 ];    
        } else {
            v2 = status.getCurrentChainObject()[ x2 ];
        }
        $('#bead-interval-pdb-start').val(v1.resName + ':' + v1.resSeq);
        $('#bead-interval-pdb-end').val(v2.resName + ':' + v2.resSeq);  

      } else {

        $('#bead-interval-pdb-info').hide();

      }

    }

    // other controls
    $('#stride-edt').val(status.getRunOptions('stride'));

  }


  self.getActiveBeadRange = function() {
    var invert = $("#bead-interval-invert").prop('checked');
    var x1 = parseInt($('#bead-interval-start').val());
    var x2 = parseInt($('#bead-interval-end').val());
    if ( invert ) [x1, x2] = [x2, x1];
    return [x1, x2];
  }

  self.clearMessageBoard = function() {
    $("#result-info").html("");
  }

  self.errorMessage = function(err, preformatted=false) {

    var ediv = '<div class="alert alert-danger" role="alert">'+ err + '</div>';
    var html = preformatted ? '<pre>'+ ediv + '</pre>' : ediv; 
    $("#result-info").append(html);
    
  }

  self.infoMessage = function(msg, preformatted=false) {

    var ediv = '<div class="alert alert-info" role="alert">'+ msg + '</div>';
    var html = preformatted ? '<pre>'+ ediv + '</pre>' : ediv; 
    $("#result-info").append(html);
    
  }

  self.setTxtDowloadLink = function( text='' ) {

    var blob = new Blob([text], {type: 'text/plain'});
    $("#download-link").on("click", function () {
      saveAs(blob, 'knot-finder-results.txt');
    });

  };

  self.setZipDownloadLink = function( data ) {

    var zip = new JSZip();
    zip.file("output.txt", data.result.out);
    zip.file("err.txt", data.result.err);
    zip.file("log.txt", data.result.log);

    $("#download-link").on("click", function () {
      
      zip.generateAsync( {type: "blob"} ).then( function ( blob ) { // 1) generate the zip file
  
          saveAs(blob, "knot-finder-results.zip");            // 2) trigger the download
  
      }, function (err) {

          self.errorMessage(err, true);
      
      });
    
    });

  };


  self.setLogs = function( data ) {

    var parmtxt = JSON.stringify( data.param, null, 2 );
    $('#log-prm').html( parmtxt );
    $('#log-cmd').html( data.cmd );
    $('#log-out').html( data.result.out );
    $('#log-err').html( data.result.err );
  
  }


  self.abortButton = function( action ) {
    
    if ( action === 'show' ){

      $('#abort-job-btn').show();

    } else if ( action === 'hide' ) {

      $('#abort-job-btn').hide();      

    }

  }

  self.getRunOptions = function() {
    
    var options = {
     
      full : $('#full-yes').prop('checked'),
      topdown : $('#topdown').prop('checked'),
      stride: $('#stride-edt').val(),
    
    }
    return options;

  }

  self.showKnotDescription = function( status ) {

    self.clearMessageBoard();

    var i = status.getCurrentIndex();
    var kt = status.knot_type[ i ];
    var txt = '';
    if ( kt === false ) {
      
      txt = '';
    
    } else if ( kt === "UN" ) {

      txt = 'No knots detected';
    
    } else {

       
      txt = 'Alexander polynomial evaluated at t=-1 and t=-2: <p class="mathtext" style="padding-left:1em;">A(-1)=' + status.alexander_dets[ i ][ 0 ] + '<br>A(-2)=' + status.alexander_dets[ i ][ 1 ] + '</p>';
      txt += '<div>Simplest knot type compatible with A(-1) and A(-2):<p>'
      if ('kt' == '??') {
        txt += '<span class="mathtext"><mark>??</mark></strong>';
      } else {
        cross_ord = kt.split('_');
        if (cross_ord[1] === undefined) {
          txt += '<span class="mathtext"><mark><strong>' + cross_ord[ 0 ] + '</strong></mark></span>';
        } else {
          txt += '<span class="mathtext"><mark><strong>' + cross_ord[ 0 ] + '<sub>' + cross_ord[ 1 ] + '</sub></strong></mark></span>';
          txt += '<small><a target="_blank" href="http://katlas.org/wiki/' + kt + '">[view on KnotAtlas]</a></small><br/>';
        }
      }
      txt += '</p></div>';
      var range = status.getKnotRange( i );
      var start = range[ 0 ], end = range[ 1 ];
      if ( status.hasPdbInfo() ) {

        var e0 = status.getCurrentChainObject()[ range[ 0 ] ];
        var e1 = status.getCurrentChainObject()[ range[ 1 ] ];
        start = e0.resName + ':' + e0.resSeq + ' (' + range[ 0 ] + ')';
        end = e1.resName + ':' + e1.resSeq + ' (' + range[ 1 ] + ')';

      }
      txt += 'Position: <p class="mathtext">' + start + ' &#8594; ' + end + '</p>';

    }

    $('#viewer-info').html(txt);

    if ( txt !== '' ) 
      self.infoMessage(txt);
    
  }

  self.drawKymo = function( status ) {

    $('.kymo').remove();
    
    var kymodata = status.trajectory_flag ? status.kymo[ 0 ] : status.kymo[ status.getCurrentIndex() ];
    if ( kymodata ) {
      
      var kymo = $('<img class="kymo" width=100% heigth=20% src=\'data:image/svg+xml;utf8,' + kymodata + '\'>');
      $('#kymo-div').append(kymo);

    }
    
  }

}
// $('button#run-job-btn').removeClass('btn-primary')
//                          .addClass('btn-warning')
//                          .html('<i class="fa fa-circle-o-notch fa-spin"></i> Finding knots...');
  
//                           .html('Find Knots')
//                          .removeClass('btn-warning')
//                          .addClass('btn-primary')

function flatten(arr) {
  return arr.reduce(function (flat, toFlatten) {
    return flat.concat(Array.isArray(toFlatten) ? flatten(toFlatten) : toFlatten);
  }, []);
}

var KnotFinderServer = function(callbacks) {

  var self = this;
  var url = 'ajax_run.php';
  var cancelUrl = 'ajax_cancel.php';
  var pollUrl = 'ajax_poll.php';
  var failedAttemptsLimit = 5;
  self.running = false;
  var failedAttempts = 0;
  var pollInterval = 1000;
  var ignoreList = {};
  self.ignoreList = ignoreList;

  self.errorHandler = function(x, err, et){

    self.running = false;
    self.onerror(self, x, err, et);

  }

  self.sendRequest = function(appstatus, callbacks) {

    var internalID = guid();

    self.running = true;
    self.currentRequest = null;
    self.currentRequestInternalID = '' + internalID;

    // set the callbacks
    var callbacks = callbacks || {}
    self.beforestart = callbacks.beforestart || function(){};
    self.sendsuccess = callbacks.sendsuccess || function(){};
    self.onfail = callbacks.fail || function(){};
    self.beforecancel = callbacks.fail || function(){};
    self.remoteError = callbacks.remoteError || function(){};
    self.jobRunning = callbacks.jobRunning || function(){};
    self.queued = callbacks.queued || function(){};
    self.finish = callbacks.finish || function(){};
    self.onupload = callbacks.onupload || function(){};
    self.oncancel = callbacks.cancel || function(){};
    

    self.onerror = callbacks.error || function(){};

    failedAttempts = 0;

    // run preparation
    self.beforestart(self);

    // prepare data
    var runOptions = appstatus.getRunOptions();
    var range = appstatus.getActiveBeadRange();
    var linear = ! appstatus.isRing();
    // if ( !linear && range[1] == appstatus.getCurrentStructure().length - 1 )
    //   range = [range[0], range[1] - 1];

    var num_frames = appstatus.num_frames;
    var coordinates = null;
    if ( appstatus.isTrajectory() ) {

      coordinates = flatten( appstatus.getTrajectory() );
    
    } else {

      coordinates = flatten( appstatus.getCurrentStructure() );
    
    }

    var num_beads = coordinates.length / 3 / num_frames;

    var data = {

      num_frames: num_frames, 
      num_beads: num_beads,
      coordinates: coordinates,
      linear: linear,
      full: runOptions.full,
      topdown: runOptions.topdown,
      stride: runOptions.stride,
      range: range,

    };

    console.log(data);
    var jsondata = { jsondata: JSON.stringify(data) };
    
    // send async request to server
    $.ajax({
      type: "POST",
      url: url,
      data: jsondata,
      success: function( data ) {

        if ( internalID in ignoreList ) return false;
        self.processResults( data, internalID );

      },
      dataType: 'json',
      error: function(x, err, et){

        if ( internalID in ignoreList ) return false;
        self.running = false;
        self.onerror(self, x, err, et);

      },
      // this stuff is to have updatable upload status
      xhr: function () {

        var jqXHR = null;
        
        jqXHR = new window.XMLHttpRequest();
        
        //Upload progress
        jqXHR.upload.addEventListener( "progress", function ( evt )
        {
            if ( internalID in ignoreList ) return false;
            if ( evt.lengthComputable )
            {
              self.onupload(evt.loaded, evt.total);
            }
        }, false );

        return jqXHR;

      },

    });

  }


  self.stopRequest = function(){
    
    if (! self.running ) return false;
    ignoreList[ self.currentRequestInternalID ] = 1;
    
    $.ajax({
      type: "POST",
      url: cancelUrl,
      data: {

        id: self.currentRequest,
        
      },
      success: function(data){

        console.log(data);
        self.running = false;
        self.oncancel(self);

      },
      dataType: 'json',
      error: function(x, err, et){

        // even if something goes wrong, we want the user to regain 
        // control of the interface. Just sweep underneath the rug.
        console.log('got error on cancel', x, err, et);
        self.running = false;
        self.oncancel(self);

        //self.onerror(self, x, err, et);

      },
    });

  }


  self.processResults = function(data, internalID){

    if ( internalID in ignoreList ) return false;

    console.log(data);
    
    if (data.status == 'ok'){ 

      self.currentRequest = data.data;
      self.sendsuccess(self);
      setTimeout(self.pollStatus( internalID ), pollInterval);

    } else {

      self.onfail(self, 'Server error', data);

    }
    
  }



  self.pollStatus = function( internalID ){

    function inner(){

      if ( internalID in ignoreList ) return false;
    
      if ( ! self.running ) return false;

      jsondata = { id: self.currentRequest };

      $.ajax({
        type: "POST",
        url: pollUrl,
        data: jsondata,
        success: self.gotStatus( internalID ),
        dataType: 'json',
        error: function(x, err, et){

          if ( internalID in ignoreList ) return false;

          self.running = false;
          if ( failedAttempts < failedAttemptsLimit ) {

            console.log(`poll request failed. Retrying (${failedAttempts}/${failedAttemptsLimit})`);
            failedAttempts++;
            setTimeout(self.pollStatus( internalID ), pollInterval);
          
          } else {
           
            self.onerror(self, x, err, et);
          
          }

        },

      });

    }

    return inner;

  }

  self.gotStatus = function ( internalID ){

    var inner = function( data ){

      if ( internalID in ignoreList ) return false;

      failedAttempts = 0;

      if ( data.status === 'ok' ) { 

        self.jobstatus = data.data;

        if ( self.jobstatus.status === 'running' ){

          self.jobRunning(self);
          setTimeout(self.pollStatus( internalID ), pollInterval);

        }

        if ( self.jobstatus.status === 'queued' ){

          self.queued(self);
          setTimeout(self.pollStatus( internalID ), pollInterval);

        }

        if ( self.jobstatus.status === 'completed' ){

          self.running = false;
          self.finish(self);

        }

        if ( self.jobstatus.status === 'error' ){


          self.remoteError(self);

        }

      } else {

        self.jobstatus = data;
        self.remoteError(self);

      }

    };

    return inner;

  } 


}

var webgl_error_message =`
  <p>
    Error. I was unable to create the WebGL window. <br/>
  </p>
  <p>
    Check this page to see if webgl is supported on your browser:
    <a href="https://get.webgl.org/">https://get.webgl.org/</a>.
  </p>
  <p><small><i>
    If you are using Google Chrome on a Mac, it may be because of your
    hardware setup. More details should be available on 
    <a href="chrome://gpu/">chrome://gpu/</a>
    You may try to run chrome manually specifying --ignore-gpu-blacklist,
    but you'll probably just want to use Safari for this website.    
  </i></small></p>`

var TubeView = function(container, options){

  var self = this;

  var container, stats;

  var camera, controls, scene, renderer;

  var cross;

  var container = container;

  var options = options || {};
  var backgroundColor = options.color || 0xeeeeee;

  var tubeOptions = options.tubeOptions || {

    splineQuality: 10,
    tubeWidth: 0.2,

  };

  var splineOptions = options.splineOptions || {

    curveType: 'catmull',

  };

  self.materials = {
    transparent: new THREE.MeshLambertMaterial({ 
      color: 0x2e9afe, 
      opacity: 0.2,
      transparent: true,
    }),

    solid: new THREE.MeshLambertMaterial({ 
      color: 0x2e9afe, 
      side: THREE.DoubleSide, 
      transparent: false, 
      reflectivity: 0,
      envMap: null,
    }),

    highlight: new THREE.MeshLambertMaterial({ 
      color: 0xfe9a2e, 
      side: THREE.DoubleSide, 
      transparent: false, 
      reflectivity: 0,
      envMap: null,
    }),

    transparent_highlight: new THREE.MeshLambertMaterial({ 
      color: 0xfe9a2e, 
      opacity: 0.2,
      transparent: true,
    }),

  };

  
  camera = new THREE.PerspectiveCamera( 50, container.offsetWidth / container.offsetHeight, 1, 10000 );
  camera.position.z = 50;

  self.group = new THREE.Group();

   // lights

  var light = new THREE.DirectionalLight( 0xffffff, 0.7);
  light.position.set( 100, 0, 50);
  camera.add( light );
  var light = new THREE.DirectionalLight( 0xffffff, 0.5);
  light.position.set( -50, -50, -50);
  camera.add( light );

  var ambient_light = new THREE.AmbientLight(0xffffff, 0.3);

  //var light = new THREE.DirectionalLight( 0x002288 );
  //light.position.set( -1, -1, -1 );
  //scene.add( light );

  //var light = new THREE.AmbientLight( 0x222222 );
  //scene.add( light );

  controls = new THREE.TrackballControls( camera, container );

  controls.rotateSpeed = 5.0;
  controls.zoomSpeed = 10.2;
  controls.panSpeed = 0.8;

  controls.noZoom = false;
  controls.noPan = false;

  controls.staticMoving = true;
  controls.dynamicDampingFactor = 0.3;

  controls.keys = [ 65, 83, 68 ];


    // world

  scene = new THREE.Scene();
  scene.background = new THREE.Color( backgroundColor );
  scene.add(camera);
  scene.add(ambient_light);
  scene.add(self.group);
  //scene.fog = new THREE.FogExp2( 0xcccccc, 0.002 );

  // renderer

  try {

    renderer = new THREE.WebGLRenderer( { antialias: true } );  
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize( container.offsetWidth, container.offsetHeight, false );
    container.appendChild( renderer.domElement );

  } catch (error) {

    $( container ).html(webgl_error_message);
    console.log( error );
  
  }
  

  function getAverageSpacing(points){
  
    var avdist = 0.0;
    for (var i = 0; i < points.length-1; ++i){

      avdist += points[i].distanceTo(points[i+1]);

    }
    return avdist/(points.length-1);
  
  }

  function crdToVec3s(coordinates){
    points = [];
    for (var i = 0; i < coordinates.length; ++i){

      var pos = coordinates[i];
      points.push(new THREE.Vector3( pos[0], pos[1], pos[2] ));

    }
    return points;
  }

  self.getCenter = function(points){

    var com = new THREE.Vector3(0, 0, 0);
    for (var i = 0; i < points.length; ++i)
      com.add(points[i]);
    return com.multiplyScalar(1.0/points.length);

  }


  self.onWindowResize = function() {

    camera.aspect = container.offsetWidth / container.offsetHeight;
    camera.updateProjectionMatrix();

    renderer.setSize( container.offsetWidth, container.offsetHeight );

    controls.handleResize();

    self.render();

  }

  self.animate = function() {

    requestAnimationFrame( self.animate );
    controls.update();

  }

  self.render = function() {
    
    if (renderer)
      renderer.render( scene, camera );

  }

  controls.addEventListener( 'change', self.render );
  window.addEventListener( 'resize', self.onWindowResize, false );


  self.render();
  self.animate();

  self.getSpline = function(coordinates, closed=false, curve_type='chordal', tension=0.5, splineFactor=3){
    
    var vecs = crdToVec3s(coordinates);
    var avedist = getAverageSpacing(vecs);
    var p = closed ? 0: 1;
    var numSplinePoints = (coordinates.length - p)*splineFactor;
    var splineWidth = avedist/16;

    var pipeSpline = new THREE.CatmullRomCurve3( vecs, closed, curve_type, tension );
    return pipeSpline

  }

  self.PartialSpline = function ( spline, start, end, length=1.0, scale=1.0) {

    THREE.Curve.call( this );
    this.scale = ( scale === undefined ) ? 1 : scale;
    this.spline = spline;
    this.start = start / length;
    this.end = end / length;
    this.range = this.end - this.start;

  }

  self.PartialSpline.prototype = Object.create( THREE.Curve.prototype );
  self.PartialSpline.prototype.constructor = self.PartialSpline;

  self.PartialSpline.prototype.getPoint = function ( t ) {
    
    return this.spline.getPoint( this.start + t*this.range ).multiplyScalar( this.scale );

  };

  self.scaleCoordinates = function(points, maxSpan){

    var bbox = new THREE.Box3().setFromPoints(points);
    var sizes = bbox.getSize();
    var maxDim = Math.max(sizes.x, sizes.y, sizes.z);
    var scale = maxSpan/maxDim;
    for (var i = 0; i < points.length; ++i){
      points[i].multiplyScalar(scale);
      
    }
  }

  self.setStructure = function(coordinates, opts){
    
    var opts = opts || {};
    self.closed = opts.closed || false;
    self.curveType = opts.curveType || 'chordal';
    self.tension = opts.tension || 0.5;
    
    // transform coordinates into points, scale and center them
    self.points = crdToVec3s(coordinates);
    self.numPoints = coordinates.length - (self.closed ? 0: 1);
    self.scaleCoordinates(self.points, 16);
    var center = self.getCenter(self.points);
    for (var i = 0; i < points.length; ++i)
      self.points[i].sub(center);

    self.pipeSpline = new THREE.CatmullRomCurve3( self.points, self.closed, self.curve_type, self.tension );

  }

  self.addTube = function(opts){

    var opts = opts || {};
    var tubeWidth = opts.tubeWidth || 0.1;
    var splineQuality = opts.splineQuality || 1;
    var materialName = opts.material || 'solid';
    var range = opts.range || [0, self.numPoints];
    var splineWidth = opts.splineWidth || 1;
    var tubeQuality = opts.tubeQuality || 10;

    if ( range[0] > self.numPoints || range[1] > self.numPoints){
      console.log('invalid range?', range, 'numPoints:', self.numPoints);
    }
    if ( range[0] > range[1] ){
      var x1 = range[1];
      var x2 = range[0];
      opts.range = [0, x1];
      self.addTube(opts);
      opts.range = [x2, self.numPoints];
      self.addTube(opts);
      return false;
    }

    // get average distances to use for the tube width
    var avedist = getAverageSpacing(self.points);
    var currTube = new self.PartialSpline(self.pipeSpline, range[0], range[1], self.numPoints);
    var currNumPoints = Math.abs(range[1] - range[0]);

    var numSplinePoints = currNumPoints * splineQuality;
    var geometry = new THREE.TubeBufferGeometry( currTube, numSplinePoints, tubeWidth*avedist, tubeQuality, false );
    var mesh = new THREE.Mesh( geometry, self.materials[materialName] );
    self.group.add(mesh);

  }

  self.clear = function(render=true){

    for ( var i=self.group.children.length -1 ; i >= 0; --i ){
      obj = self.group.children[i];
      self.group.remove(obj);
    }
    if ( render )
      self.render();

  }

  self.redraw = function(status, updateStructure=false) {

    self.clear(false); 
    if (updateStructure){
      
      var crd = status.getCurrentStructure();
      splineOptions.closed = isRing(crd);
      self.setStructure(crd, splineOptions);
      
    }
      
    if ( ! status.num_beads ) {

      console.log('No beads to draw');
      return false;

    }
    // stack annotations
    var activeRange = status.getActiveBeadRange();
    var knotRange = status.getKnotRange();
    if ( knotRange === false ) {
      // no knot detected, set to [0, 0]: will not be drawn.
      knotRange = [0, 0];
    }
    var drawRanges = stackRanges(['knot', 'active', 'all'], [knotRange, activeRange, [0, status.num_beads-1]], status.num_beads-1);

    if (self.points){

      for (var i = 0; i < drawRanges.length; i++) {
        var keys = drawRanges[i][0];
        var start = drawRanges[i][1];
        var stop = drawRanges[i][2];
        var opts = deepcopy(tubeOptions);
        opts.range = [start, stop];
        var knot = keys.indexOf('knot') !== -1;
        var active = keys.indexOf('active') !== -1;
      
        // select material  
        if (knot && active){
          opts.material = 'highlight';  
        } else if (knot && !active) {
          opts.material = 'transparent_highlight';
        } else if (!knot && active) {
          opts.material = 'solid';
        } else if (!knot && !active) {
          opts.material = 'transparent';
        }
      
        self.addTube(opts);  
        
      }
      
      
    }
    
    self.render();

  }


  this.scene = function(){return scene;}
  this.renderer = function(){return renderer;}
  this.camera = function(){return camera;}
  this.controls = function(){return controls;}

}
function sleep(millis)
{
    var date = new Date();
    var curDate = null;
    do { curDate = new Date(); }
    while(curDate-date < millis);
}

var KnotFinderApp = function(){

  var self = this;

  // set some constants
  var sliderOn = "rgba(0, 0, 0, 0) linear-gradient(rgb(209, 209, 209) 0px, rgb(205, 205, 205) 100%) repeat-x";
  var sliderOff = "rgba(0,0,0,0)";

  // set the size for the viewer
  var el = $('#viewer');  //record the elem so you don't crawl the DOM everytime
  var bottom = el.offset().top + el.outerHeight(true);
  var height = $(window).height() - bottom;
  var container = document.getElementById('viewer')
  container.setAttribute("style","height:" + Math.round(height*0.7) + "px");

  // create the viewer with default options
  var viewer = new TubeView(container);

  // create the app status
  var status = new AppStatus();

  var interface = new InterfaceControl();

  var server = new KnotFinderServer();

  this.viewer = viewer;
  this.status = status;
  this.server = server;
  this.interface = interface;

  // sets up the file loader

  var fileField = document.getElementById('file-field');
  var fileLoader = new KnotFileLoader(fileField, {

    beforestart: function(){

      interface.deactivate();

    },

    onerror : function(e){

      alert(e.error);
      interface.activate();

    },

    onload: function(e){
      try{
        // update status

        if ( e.dataType === 'pdb' )
          status.setChains(e.data);
        else
          status.setTrajectory(e.data);

        // update ui
        
        status.setRunOptions({ stride: parseInt(status.num_beads / 50) })
        
        // update view
        viewer.redraw(status, true);
        interface.drawKymo( status );
        interface.showKnotDescription( status );
        interface.updateControls(status, 'all');
        $("#upload-dialog").modal("hide");
        interface.activate();
      }
      catch(err) {
        alert(err.message);
      }

    }

  });
  
  self.fileLoader = fileLoader;

  
  self.changeBeadInterval = function(){

    [x1, x2] = interface.getActiveBeadRange();

    // update status
    status.setActiveBeadRange(x1, x2);

    // update view
    viewer.redraw(status, false);

    // update ui
    interface.updateControls(status, 'bead-range');

  }
  

  self.changeFrame = function( i ) {

    if ( i < 0 || i >= status.num_frames )
      return false;

    interface.deactivate();  
    $('#vv-load-screen').show();
    let p = new Promise( function(resolve, reject) {
      setTimeout( function() {
        try {
          console.log('called');
          status.setCurrentFrame( i );
          viewer.redraw(status, true);
          interface.showKnotDescription( status );
          interface.updateControls(status, 'traj');
          interface.drawKymo( status );
          resolve('');
        } 
        catch (err) {
          reject(err.message);
        }
  
      }, 0);
    });
      
    p.then(
      function(m) {
        interface.activate();
        $('#vv-load-screen').hide();
      }, 
      function(m) {
        console.log(m);
        alert(m);
    });
    
  }

  self.changeChain = function( i ) {

    var i = ( i === undefined ) ? parseInt($('#chain-selector').val()) : i;

    if ( i < 0 || i >= status.num_chains )
      return false;
      
    status.setCurrentChain( i );

    interface.showKnotDescription( status );

    interface.updateControls(status, 'bead-range');

    interface.drawKymo( status );

    viewer.redraw(status, true);
    
  }


  self.updateStatusFromData = function( data, set_download=false ) {

    var knots = data.knots || [];
    if (data.output && set_download) 
      interface.setTxtDowloadLink( data.output );

    if ( status.trajectory_flag ) {
            
      for ( var i = 0; i < knots.length; i++ ) {

        status.setKnotRange( knots[ i ].start, knots[ i ].end, i ); 
        status.knot_type[ i ] = knots[ i ].name; 
        status.alexander_dets[ i ] = [ knots[ i ].Adet1, knots[ i ].Adet2 ];
      
      }

      if ( data.kymo )
        status.kymo[ 0 ] = data.kymo;

    } else {
    
      var i = status.getCurrentIndex();
      
      if ( knots.length ) {

        status.setKnotRange( knots[ 0 ].start, knots[ 0 ].end, i ); 
        status.knot_type[ i ] = knots[ 0 ].name; 
        status.alexander_dets[ i ] = [ knots[ 0 ].Adet1, knots[ 0 ].Adet2 ];
        
      }

      if ( data.kymo )
        status.kymo[ i ] = data.kymo;
    
    }

  }

  self.requestJob = function() {

    status.setRunOptions(interface.getRunOptions());

    server.sendRequest(status, {
      
      beforestart: function() {
      
        interface.clearMessageBoard();
        interface.deactivate();
        interface.infoMessage('Sending job...');
        interface.abortButton('show');
      
      },

      remoteError: function( server ) {

        var err = server.jobstatus.reason || '';
        interface.clearMessageBoard();

        var error_message = '<strong>Job Failed</strong><br/>' +
                            err.replace(/\\n/g, "<br>");;
        interface.errorMessage(error_message);
        interface.activate();
        interface.abortButton('hide');

      },

      queued: function( server ) {

        interface.clearMessageBoard();
        interface.infoMessage('The job has been queued, and will start shortly.');

      },

      jobRunning: function( server ) {

        var data = server.jobstatus.data;
        var progress = 0;
        if ( data.knots ){

          progress = data.knots.length / status.num_frames;
        
          self.updateStatusFromData( data );

          // add the kymo to the bottom
          interface.drawKymo( status );
          interface.showKnotDescription( status );

          viewer.redraw( status );

        }

        interface.clearMessageBoard();
        interface.infoMessage('<i class="fas fa-circle-notch fa-spin"></i> Hold on, job is running. ' + ( progress*100 ).toFixed(0) + '% completed.' );
        
      },

      sendsuccess: function( server ) {

        interface.clearMessageBoard();
        interface.infoMessage('Working...');
        // status.setKnotInfo(data);
        // interface.setLogs(data);
        // interface.setDownloadLink(data);
        // interface.activate();
        // interface.abortButton('hide');
        // viewer.redraw(false);

      },

      fail: function( server, reason, data) {

        var reason = data.reason || '';
        var err = reason.replace(/\\n/g, "<br>");
        interface.clearMessageBoard();
        var error_message = '<strong>Job Failed</strong><br/>' +
                            reason +
                            '<br/><strong>Standard Error:</strong><pre>' + 
                            err +
                            '</pre></div>';
        interface.errorMessage(error_message);
        interface.activate();
        interface.abortButton('hide');

      },

      cancel: function( server ) {

        interface.clearMessageBoard();
        interface.infoMessage('Job aborted');
        interface.activate();
        interface.abortButton('hide');

      }, 

      error: function( server, x, err, et) {

        var err = err.replace(/\\n/g, "<br>");
        var error_message = `<strong>Communication error</strong><br/>
                            <p>The client failed to communicate with the server
                            multiple times. This may be due to a connection issue,
                            or eccessive server workload may be slowing the server down.
                            Retry in a few minutes.</p>
                            <i>XHR error: ${err}</i>`;
        interface.errorMessage(error_message);
        console.log(x, err, et);
        interface.activate();
        interface.abortButton('hide');

      },

      finish: function( server ) {

        interface.activate();
        interface.abortButton('hide');
        interface.clearMessageBoard();

        var data = server.jobstatus.data;
        console.log(data);
        
        if (data.returncode === 0) {
        
          self.updateStatusFromData( data, true );

          // add the kymo to the bottom
          interface.drawKymo( status );
          interface.showKnotDescription( self.status );
        
        } else { // some error occourred

          var err = data.warning.replace(/\\n/g, "<br>");
          interface.errorMessage( '<strong>Internal server error:</strong><br/>' + err );

        }
        
        viewer.redraw(status);

      },

      onupload: function( loaded, total ) {

        var progress = Math.round( loaded * 100.0 / total);
        interface.clearMessageBoard();
        if ( progress == 100 ) {
          interface.infoMessage('Upload completed, processing...');
        } else {
          interface.infoMessage('Hold on, uploading data: ' + progress + '%');
        }
      }

    });

  }

  self.abortJob = function() {

    server.stopRequest();

  }

  self.changeRunOptions = function(){

    opts = interface.getRunOptions();
    status.setRunOptions(opts);

  }

  self.changeClosure = function( linear=true ) {

    var cb = linear ? status.openRing : status.closeRing;

    if ( status.isTrajectory() ) {

      for (var i = 0; i < status.num_frames; i++) 
        cb(i);

    } else {

      cb( status.getCurrentIndex() );
    
    }

    interface.updateControls(status, 'bead-range');
    viewer.redraw(status, true);

  }

}

var app = null;
var load_screen = null;

$(document).ready(function() {

  // create the loading screen
  // load_screen = $(`
  //   <div id="load-screen" style="position:absolute; top:0; left:0; width:100vw; height:100vh; background-color: rgba(0,0,0,.4)">
  //     <span style="position:absolute; top:40%;left:40%; font-size:50px;">
  //       <i class="fa fa-circle-o-notch fa-spin"></i>
  //     </span>
  //   </div>`);
  // $('body').append(load_screen);

  // setup the app 
  app = new KnotFinderApp();
  
  $('#viewer-help > a').tooltip(); 

  // actually disable links
  $(".toolbar-link.disabled").prop('disabled', true);
  
  // 
  $("#bead-interval-invert").on("change", function() {

    app.changeBeadInterval(); 
    
  });

  $("#traj-next").on('click', function() {

    app.changeFrame( app.status.current_frame + 1 );

  });

  $("#traj-prev").on('click', function() {

    app.changeFrame( app.status.current_frame - 1 );

  });

  $('#bead-interval-start, #bead-interval-end').on("change", function(){

    app.changeBeadInterval();

  });

  window.scrollTo(0, 0);

  $('input[name=ring]').on('change', function() {
  
    var selValue = $('input[name=ring]:checked').val();
    app.changeClosure( selValue === 'linear' );
  
  });

  $('#chain-selector').on('change', function(){
    
    app.changeChain();
  
  });
  
  $('button#run-job-btn').on("click", function(){

    app.requestJob();

  });

  $('button#abort-job-btn').on("click", function(){

    app.abortJob();

  });

  $('#slider').on("slideStop", function(evt){

    var value = evt.value;
    $('#bead-interval-start').val(value[0]);
    $('#bead-interval-end').val(value[1]);
    app.changeBeadInterval();

  });

  // show the upload dialog as soon as the page is ready.
  if( app && app.viewer && app.viewer.renderer() ) {
    $('#upload-dialog').modal("show");
  }

  if( ! app.viewer.renderer() ) {
    $('#viewer-help').hide();
  }

  $("button#select-file-btn").click(function(){
    
    if ( document.getElementById('file-field').files.length != 0 )
      app.fileLoader.startRead();
    return false;

  });
    
  window.onbeforeunload = function (e) {

      if (app.status.all_coordinates.length > 0)
      {
        e = e || window.event;

        // For IE and Firefox prior to version 4
        if (e) {
            e.returnValue = 'Sure?';
        }

        // For Safari
        return 'Sure?';
      } 

  };

});

// $(window).on('load', function() { 
  
//   $('#load-screen').hide();

// });



