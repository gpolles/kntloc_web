var app = null;
var load_screen = null;

$(document).ready(function() {

  // create the loading screen
  // load_screen = $(`
  //   <div id="load-screen" style="position:absolute; top:0; left:0; width:100vw; height:100vh; background-color: rgba(0,0,0,.4)">
  //     <span style="position:absolute; top:40%;left:40%; font-size:50px;">
  //       <i class="fa fa-circle-o-notch fa-spin"></i>
  //     </span>
  //   </div>`);
  // $('body').append(load_screen);

  // setup the app 
  app = new KnotFinderApp();
  
  $('#viewer-help > a').tooltip(); 

  // actually disable links
  $(".toolbar-link.disabled").prop('disabled', true);
  
  // 
  $("#bead-interval-invert").on("change", function() {

    app.changeBeadInterval(); 
    
  });

  $("#traj-next").on('click', function() {

    app.changeFrame( app.status.current_frame + 1 );

  });

  $("#traj-prev").on('click', function() {

    app.changeFrame( app.status.current_frame - 1 );

  });

  $('#bead-interval-start, #bead-interval-end').on("change", function(){

    app.changeBeadInterval();

  });

  window.scrollTo(0, 0);

  $('input[name=ring]').on('change', function() {
  
    var selValue = $('input[name=ring]:checked').val();
    app.changeClosure( selValue === 'linear' );
  
  });

  $('#chain-selector').on('change', function(){
    
    app.changeChain();
  
  });
  
  $('button#run-job-btn').on("click", function(){

    app.requestJob();

  });

  $('button#abort-job-btn').on("click", function(){

    app.abortJob();

  });

  $('#slider').on("slideStop", function(evt){

    var value = evt.value;
    $('#bead-interval-start').val(value[0]);
    $('#bead-interval-end').val(value[1]);
    app.changeBeadInterval();

  });

  // show the upload dialog as soon as the page is ready.
  if( app && app.viewer && app.viewer.renderer() ) {
    $('#upload-dialog').modal("show");
  }

  if( ! app.viewer.renderer() ) {
    $('#viewer-help').hide();
  }

  $("button#select-file-btn").click(function(){
    
    if ( document.getElementById('file-field').files.length != 0 )
      app.fileLoader.startRead();
    return false;

  });
    
  window.onbeforeunload = function (e) {

      if (app.status.all_coordinates.length > 0)
      {
        e = e || window.event;

        // For IE and Firefox prior to version 4
        if (e) {
            e.returnValue = 'Sure?';
        }

        // For Safari
        return 'Sure?';
      } 

  };

});

// $(window).on('load', function() { 
  
//   $('#load-screen').hide();

// });



