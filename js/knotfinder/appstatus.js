

var AppStatus = function(){

  var self = this;

  self.all_coordinates = []; // all loaded coordinates
  self.chains = []; // structures holding additional information from pdb
  
  self.num_beads = 0; self.num_chains = 0; self.num_frames = 0;
  self.current_chain = 0; self.current_frame = 0; 

  self.trajectory_flag = false; self.pdb_flag = false; self.ring_polymer_flag = false;

  self.active_beads_range = false;

  self.knot_positions = [];

  self.apply_closure = [];

  self.runOptions = {

    full: false,
    topdown : false,
    stride: -1,

  }
  self.kymo = [ false ];

  this.updateInternals = function(){


    self.num_chains = self.chains.length;
    self.num_frames = self.num_chains ? 1 : self.all_coordinates.length;
    self.num_beads = self.all_coordinates.length > 0 ? self.all_coordinates[0].length : 0;
    
    self.current_frame = 0;
    self.current_chain = 0;

    self.trajectory_flag = self.num_frames > 1;
    self.pdb_flag = self.num_chains > 0;
    self.ring_polymer_flag = ( self.num_beads > 0 ) ? isRing( self.all_coordinates[0] ) : false;

    self.kymo = self.trajectory_flag ? [ false ] : new Array( self.num_chains );

    self.active_beads_range = [0, self.num_beads - 1];

    self.knot_positions = new Array( self.num_chains || self.num_frames );
    self.knot_positions.fill(false);

    self.knot_type = new Array( self.num_chains || self.num_frames );
    self.knot_type.fill(false);

    self.alexander_dets = new Array( self.num_chains || self.num_frames );
    self.alexander_dets.fill(false);

    self.apply_closure = new Array( self.num_chains || self.num_frames );
    self.apply_closure.fill(false);

  }

  self.setTrajectory = function(coordinates){
    if (coordinates === null){

      console.log('error on setTrajectory');
      return false;

    }
    self.chains = [];
    self.all_coordinates = coordinates;
    self.updateInternals();
  }

  self.getTrajectory = function(){
    
    if ( self.all_coordinates.length && self.isTrajectory() ){

      return self.all_coordinates;

    }

    return false;

  }

  self.setChains = function(chains){
    self.chains = chains;
    self.all_coordinates = [];
    for (var i = 0; i < self.chains.length; i++) {
      self.all_coordinates.push(self.chains[i].coordinates);
    }
    self.updateInternals();
  }

  self.getBeadPdbInfo = function( i ) {

    if ( self.pdb_flag )
      return self.chains[ self.current_chain ][ i ].resSeq;
  }

  self.setCurrentFrame = function( i ) {

    if ( i >= 0 || i < self.num_frames )
      self.current_frame = i;
    return self.current_frame;
  
  }

  self.setCurrentChain = function( i ) {

    if ( i >= 0 || i < self.num_chains )
      self.current_chain = i;

    self.num_beads = self.all_coordinates[ i ].length;
    self.active_beads_range = [0, self.num_beads - 1];
    return self.current_chain;
  
  }

  self.getCurrentChain = function() {

    return self.current_chain;
  
  }

  self.getCurrentChainObject = function() {

    return self.chains[ self.current_chain ];
  
  }

  self.closeRing = function( struct_id ) {

    if (! isRing( self.all_coordinates[ struct_id ] ) ) {

      self.apply_closure[ struct_id ] = true;
      self.all_coordinates[ struct_id ].push( self.all_coordinates[ struct_id ][ 0 ] );
      if ( struct_id == self.getCurrentIndex() ) {

        self.num_beads = self.all_coordinates[ struct_id ].length;
        self.active_beads_range = [0, self.num_beads-1];
        
      } 
      
    }
    
  }

  self.openRing = function( struct_id ) {

    if ( isRing( self.all_coordinates[ struct_id ] ) ) {
      
      self.apply_closure[ struct_id ] = false;
      self.all_coordinates[ struct_id ].pop();
      if ( struct_id == self.getCurrentIndex() ) {

        self.num_beads = self.all_coordinates[ struct_id ].length;
        self.active_beads_range = [0, self.num_beads-1];
        
      } 

    }

  }

  self.getCurrentStructure = function() {
    
    return self.all_coordinates[ self.getCurrentIndex() ];
  
  }

  self.setActiveBeadRange = function(x1, x2) {

    var x1 = x1 || 0;
    var x2 = (x2 === undefined) ? self.num_beads - 1 : x2;
    
    if ( x1 < 0 || x2 < 0 || x1 >= self.num_beads || x2 >= self.num_beads ){

      console.log('invalid range', x1, x2);
      return false;

    }

    self.active_beads_range = [x1, x2];

  }

  self.getActiveBeadRange = function() {

    return ( self.active_beads_range || [0, self.num_beads] );

  }

  self.setKnotRange = function(x1, x2, struct_id=0) {

    self.knot_positions[ struct_id ] = [parseInt(x1), parseInt(x2)];

  }
 
  self.getKnotRange = function(struct_id=false) {

    if ( struct_id === false ) {

      struct_id = self.getCurrentIndex();
      
    }

    return self.knot_positions[ struct_id ];

  } 

  self.getCurrentIndex = function(){

    if ( self.trajectory_flag ) 
      return self.current_frame;
    else 
      return self.current_chain;
  
  }

  self.isTrajectory = function () {
    
    return self.trajectory_flag;

  }

  self.isRing = function() {
    
    return isRing( self.all_coordinates[ self.getCurrentIndex() ] );
  
  }

  self.setRunOptions = function( opts ) {

    Object.assign(self.runOptions, opts);

  }

  self.getRunOptions = function( name ) {

    if ( name ){

      return self.runOptions[ name ];

    } 

    return self.runOptions;

  }


  self.setKnotInfo = function( data ) {

    if ( data.num_frames !== self.num_frames ) {
      console.log('data.num_frames !== self.num_frames', data.num_frames, self.num_frames);
      return false;
    }

    var r = data.results;

    for (var i = 0; i < self.num_frames; i++) {
      
      self.knot_positions[i] = [ r.pos[ i ][ 0 ], r.pos[ i ][ 1 ] ];
      self.knot_type[i] = r.ktype[ i ];

    }

  }

  self.hasPdbInfo = function() {

    return self.pdb_flag;

  }

}