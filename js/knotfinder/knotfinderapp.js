function sleep(millis)
{
    var date = new Date();
    var curDate = null;
    do { curDate = new Date(); }
    while(curDate-date < millis);
}

var KnotFinderApp = function(){

  var self = this;

  // set some constants
  var sliderOn = "rgba(0, 0, 0, 0) linear-gradient(rgb(209, 209, 209) 0px, rgb(205, 205, 205) 100%) repeat-x";
  var sliderOff = "rgba(0,0,0,0)";

  // set the size for the viewer
  var el = $('#viewer');  //record the elem so you don't crawl the DOM everytime
  var bottom = el.offset().top + el.outerHeight(true);
  var height = $(window).height() - bottom;
  var container = document.getElementById('viewer')
  container.setAttribute("style","height:" + Math.round(height*0.7) + "px");

  // create the viewer with default options
  var viewer = new TubeView(container);

  // create the app status
  var status = new AppStatus();

  var interface = new InterfaceControl();

  var server = new KnotFinderServer();

  this.viewer = viewer;
  this.status = status;
  this.server = server;
  this.interface = interface;

  // sets up the file loader

  var fileField = document.getElementById('file-field');
  var fileLoader = new KnotFileLoader(fileField, {

    beforestart: function(){

      interface.deactivate();

    },

    onerror : function(e){

      alert(e.error);
      interface.activate();

    },

    onload: function(e){
      try{
        // update status

        if ( e.dataType === 'pdb' )
          status.setChains(e.data);
        else
          status.setTrajectory(e.data);

        // update ui
        
        status.setRunOptions({ stride: parseInt(status.num_beads / 50) })
        
        // update view
        viewer.redraw(status, true);
        interface.drawKymo( status );
        interface.showKnotDescription( status );
        interface.updateControls(status, 'all');
        $("#upload-dialog").modal("hide");
        interface.activate();
      }
      catch(err) {
        alert(err.message);
      }

    }

  });
  
  self.fileLoader = fileLoader;

  
  self.changeBeadInterval = function(){

    [x1, x2] = interface.getActiveBeadRange();

    // update status
    status.setActiveBeadRange(x1, x2);

    // update view
    viewer.redraw(status, false);

    // update ui
    interface.updateControls(status, 'bead-range');

  }
  

  self.changeFrame = function( i ) {

    if ( i < 0 || i >= status.num_frames )
      return false;

    interface.deactivate();  
    $('#vv-load-screen').show();
    let p = new Promise( function(resolve, reject) {
      setTimeout( function() {
        try {
          console.log('called');
          status.setCurrentFrame( i );
          viewer.redraw(status, true);
          interface.showKnotDescription( status );
          interface.updateControls(status, 'traj');
          interface.drawKymo( status );
          resolve('');
        } 
        catch (err) {
          reject(err.message);
        }
  
      }, 0);
    });
      
    p.then(
      function(m) {
        interface.activate();
        $('#vv-load-screen').hide();
      }, 
      function(m) {
        console.log(m);
        alert(m);
    });
    
  }

  self.changeChain = function( i ) {

    var i = ( i === undefined ) ? parseInt($('#chain-selector').val()) : i;

    if ( i < 0 || i >= status.num_chains )
      return false;
      
    status.setCurrentChain( i );

    interface.showKnotDescription( status );

    interface.updateControls(status, 'bead-range');

    interface.drawKymo( status );

    viewer.redraw(status, true);
    
  }


  self.updateStatusFromData = function( data, set_download=false ) {

    var knots = data.knots || [];
    if (data.output && set_download) 
      interface.setTxtDowloadLink( data.output );

    if ( status.trajectory_flag ) {
            
      for ( var i = 0; i < knots.length; i++ ) {

        status.setKnotRange( knots[ i ].start, knots[ i ].end, i ); 
        status.knot_type[ i ] = knots[ i ].name; 
        status.alexander_dets[ i ] = [ knots[ i ].Adet1, knots[ i ].Adet2 ];
      
      }

      if ( data.kymo )
        status.kymo[ 0 ] = data.kymo;

    } else {
    
      var i = status.getCurrentIndex();
      
      if ( knots.length ) {

        status.setKnotRange( knots[ 0 ].start, knots[ 0 ].end, i ); 
        status.knot_type[ i ] = knots[ 0 ].name; 
        status.alexander_dets[ i ] = [ knots[ 0 ].Adet1, knots[ 0 ].Adet2 ];
        
      }

      if ( data.kymo )
        status.kymo[ i ] = data.kymo;
    
    }

  }

  self.requestJob = function() {

    status.setRunOptions(interface.getRunOptions());

    server.sendRequest(status, {
      
      beforestart: function() {
      
        interface.clearMessageBoard();
        interface.deactivate();
        interface.infoMessage('Sending job...');
        interface.abortButton('show');
      
      },

      remoteError: function( server ) {

        var err = server.jobstatus.reason || '';
        interface.clearMessageBoard();

        var error_message = '<strong>Job Failed</strong><br/>' +
                            err.replace(/\\n/g, "<br>");;
        interface.errorMessage(error_message);
        interface.activate();
        interface.abortButton('hide');

      },

      queued: function( server ) {

        interface.clearMessageBoard();
        interface.infoMessage('The job has been queued, and will start shortly.');

      },

      jobRunning: function( server ) {

        var data = server.jobstatus.data;
        var progress = 0;
        if ( data.knots ){

          progress = data.knots.length / status.num_frames;
        
          self.updateStatusFromData( data );

          // add the kymo to the bottom
          interface.drawKymo( status );
          interface.showKnotDescription( status );

          viewer.redraw( status );

        }

        interface.clearMessageBoard();
        interface.infoMessage('<i class="fas fa-circle-notch fa-spin"></i> Hold on, job is running. ' + ( progress*100 ).toFixed(0) + '% completed.' );
        
      },

      sendsuccess: function( server ) {

        interface.clearMessageBoard();
        interface.infoMessage('Working...');
        // status.setKnotInfo(data);
        // interface.setLogs(data);
        // interface.setDownloadLink(data);
        // interface.activate();
        // interface.abortButton('hide');
        // viewer.redraw(false);

      },

      fail: function( server, reason, data) {

        var reason = data.reason || '';
        var err = reason.replace(/\\n/g, "<br>");
        interface.clearMessageBoard();
        var error_message = '<strong>Job Failed</strong><br/>' +
                            reason +
                            '<br/><strong>Standard Error:</strong><pre>' + 
                            err +
                            '</pre></div>';
        interface.errorMessage(error_message);
        interface.activate();
        interface.abortButton('hide');

      },

      cancel: function( server ) {

        interface.clearMessageBoard();
        interface.infoMessage('Job aborted');
        interface.activate();
        interface.abortButton('hide');

      }, 

      error: function( server, x, err, et) {

        var err = err.replace(/\\n/g, "<br>");
        var error_message = `<strong>Communication error</strong><br/>
                            <p>The client failed to communicate with the server
                            multiple times. This may be due to a connection issue,
                            or eccessive server workload may be slowing the server down.
                            Retry in a few minutes.</p>
                            <i>XHR error: ${err}</i>`;
        interface.errorMessage(error_message);
        console.log(x, err, et);
        interface.activate();
        interface.abortButton('hide');

      },

      finish: function( server ) {

        interface.activate();
        interface.abortButton('hide');
        interface.clearMessageBoard();

        var data = server.jobstatus.data;
        console.log(data);
        
        if (data.returncode === 0) {
        
          self.updateStatusFromData( data, true );

          // add the kymo to the bottom
          interface.drawKymo( status );
          interface.showKnotDescription( self.status );
        
        } else { // some error occourred

          var err = data.warning.replace(/\\n/g, "<br>");
          interface.errorMessage( '<strong>Internal server error:</strong><br/>' + err );

        }
        
        viewer.redraw(status);

      },

      onupload: function( loaded, total ) {

        var progress = Math.round( loaded * 100.0 / total);
        interface.clearMessageBoard();
        if ( progress == 100 ) {
          interface.infoMessage('Upload completed, processing...');
        } else {
          interface.infoMessage('Hold on, uploading data: ' + progress + '%');
        }
      }

    });

  }

  self.abortJob = function() {

    server.stopRequest();

  }

  self.changeRunOptions = function(){

    opts = interface.getRunOptions();
    status.setRunOptions(opts);

  }

  self.changeClosure = function( linear=true ) {

    var cb = linear ? status.openRing : status.closeRing;

    if ( status.isTrajectory() ) {

      for (var i = 0; i < status.num_frames; i++) 
        cb(i);

    } else {

      cb( status.getCurrentIndex() );
    
    }

    interface.updateControls(status, 'bead-range');
    viewer.redraw(status, true);

  }

}

