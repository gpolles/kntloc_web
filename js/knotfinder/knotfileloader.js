
var KnotFileLoader = function(domfield, opt){

  var self = this;
  var opt = opt || {};

  self.domfield = domfield;
  self.fileExtension = null;
  self.maxFileSize = opt.maxFileSize || 80*1024*1024;
  self.data = null;
  self.dataType = null;
  
  self.beforestart = opt.beforestart || function() {};
  self.onerror = opt.onerror || function() {};
  self.onprogress = opt.onprogress || function() {};
  self.onload = opt.onload || function() {};

  self.startRead = function () {
      // obtain input element through DOM
    self.beforestart(self);
    var file = self.domfield.files[0];
    self.fileExtension = file.name.split('.').pop();
    if(file){
      if (file.size > self.maxFileSize){
        self.errorHandler("Sorry, due to the limited web resources the file you choose is too large to be processed interactively." +
                          "\nPlease select a smaller file or download and run the software locally from the download page.")
        return false;
      }
      self.getAsText(file);
    }
  }

  self.getAsText = function(readFile) {

    var reader = new FileReader();
    // Read file into memory as UTF-8
    reader.readAsText(readFile, "UTF-8");

    // Handle progress, success, and errors
    reader.onprogress = self.onprogress;
    reader.onload = self.loaded;
    reader.onerror = self.errorHandler;

  }

  self.errorHandler = function(evt) {
    self.error = evt;
    self.onerror(self);
  }


  self.loaded = function (evt) {

    // Obtain the read file data
    var fileContent = evt.target.result;
    // Handle UTF-16 file dump
    self.parseContent(fileContent);
  
  }

  self.parseContent = function (fileContent) {

    var coordinates = null;

    if ( self.fileExtension == 'pdb' ) {
    
      self.data = parsePDB(fileContent, true);
      if ( self.data.length === 0 ) {
      
        self.errorHandler('Error reading coordinates (does the PDB file contain CA atoms?)');
        return false;
      
      } 
      self.dataType = 'pdb';
    
    } else {
      
      self.data = parseKNT(fileContent);
      
      if ( !self.data ){
      
        self.errorHandler('Error reading NXYZ coordinates');
        return false;
      
      }

      self.dataType = 'knt';

    }

    self.onload(self);
      
  }

}