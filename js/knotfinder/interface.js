
var InterfaceControl = function() {

  var self = this;
  var slider = $("input#slider-range").slider({ id: "slider", min: 0, max: 0, range: true, value: [0, 0] });
  var sliderOn = "rgba(0, 0, 0, 0) linear-gradient(rgb(209, 209, 209) 0px, rgb(205, 205, 205) 100%) repeat-x";
  var sliderOff = "rgba(0,0,0,0)";

  self.deactivate = function() {
  
    // disable all action while computing
    $("button#select-file-btn").prop('disabled', true);
    $("#download-link").off("click");
    $('button#run-job-btn').prop('disabled', true);
    $(".toolbar-link").prop('disabled', true);
    $(".toolbar-link:not(.disabled)").addClass('disabled');
    $("input, select").prop('disabled', true);
    slider.slider("disable");
    $("#traj-next, #traj-prev").prop('disabled', true);

  }

  self.activate = function() {

    $("button#select-file-btn").prop('disabled', false);
    $('button#run-job-btn').prop('disabled', false);
    $(".toolbar-link").prop('disabled', false);
    $('.nav-link.disabled').removeClass("disabled");
    $("input, select").prop('disabled', false);
    slider.slider("enable");
    $("#traj-next, #traj-prev").prop('disabled', false);

  }

  self.updateControls = function(status, selector){

    // show controls
    if ( status.num_beads > 0 )
      $('div#control-window').removeClass("invisible");
    else
      $('div#control-window').addClass("invisible");

    // trajectory controls
    if ( selector === 'all' || selector === 'traj' ) {

      if ( status.num_frames > 1 )
        $('#trajectory-controls').show();
      else
        $('#trajectory-controls').hide();
      $('#traj-frame').html( 'Frame ' + ( status.current_frame + 1 ) + ' of ' + status.num_frames );

    } 

    // chain controls
    if ( selector === 'all' || selector === 'chain' ) {

      if ( status.num_chains > 1 ){
        
        var select = $('#chain-selector');
        select.empty(); // remove old options
        for (var i = 0; i < status.chains.length; i++) {
        
          select.append($("<option></option>").attr("value", i)
                                              .text(status.chains[i].chainName + ' (' + 
                                                    status.chains[i].length + ' points)'));
        
        }
        select.val(status.getCurrentChain());
        $('#chain-selector-form').show();

      } else {

        $('#chain-selector-form').hide();

      }

      if ( status.ring_polymer_flag ) {
        $('#ring-controls').hide();
      } else {
        $('#ring-controls').show();
      }

    }

    // bead range controls
    if ( selector === 'all' || selector === 'bead-range' ) {

      var beadRange = status.getActiveBeadRange();

      var x1 = Math.min(beadRange[ 0 ], beadRange[ 1 ]);
      var x2 = Math.max(beadRange[ 0 ], beadRange[ 1 ]);
      var invert = beadRange[ 0 ] > beadRange[ 1 ];


      $("#bead-interval-invert").prop('checked', invert);
      $("#bead-interval-start").attr("max", status.num_beads - 1);
      $("#bead-interval-end").attr("max", status.num_beads - 1);
      $('#bead-interval-start').val(x1);
      $('#bead-interval-end').val(x2);      
      slider.slider('setAttribute', 'max', status.num_beads -1);
      slider.slider('setValue', [x1, x2]);
      $('#bead-interval-label1').html((invert ? 'End' : 'Begin'));
      $('#bead-interval-label2').html((invert ? 'Begin' : 'End'));
      $("#slider .slider-selection").css("background", 
                                         invert ? sliderOff : sliderOn);  
      $("#slider .slider-track-low," +
        "#slider .slider-track-high").css("background", 
                                          invert ? sliderOn : sliderOff);

      if ( status.hasPdbInfo() ) {

        $('#bead-interval-pdb-info').show();
        var v1 = status.getCurrentChainObject()[ x1 ];
        var v2 = null;
        if ( x2 == status.num_beads - 1 && 
             status.apply_closure[status.getCurrentIndex()] ){
            v2 = status.getCurrentChainObject()[ 0 ];    
        } else {
            v2 = status.getCurrentChainObject()[ x2 ];
        }
        $('#bead-interval-pdb-start').val(v1.resName + ':' + v1.resSeq);
        $('#bead-interval-pdb-end').val(v2.resName + ':' + v2.resSeq);  

      } else {

        $('#bead-interval-pdb-info').hide();

      }

    }

    // other controls
    $('#stride-edt').val(status.getRunOptions('stride'));

  }


  self.getActiveBeadRange = function() {
    var invert = $("#bead-interval-invert").prop('checked');
    var x1 = parseInt($('#bead-interval-start').val());
    var x2 = parseInt($('#bead-interval-end').val());
    if ( invert ) [x1, x2] = [x2, x1];
    return [x1, x2];
  }

  self.clearMessageBoard = function() {
    $("#result-info").html("");
  }

  self.errorMessage = function(err, preformatted=false) {

    var ediv = '<div class="alert alert-danger" role="alert">'+ err + '</div>';
    var html = preformatted ? '<pre>'+ ediv + '</pre>' : ediv; 
    $("#result-info").append(html);
    
  }

  self.infoMessage = function(msg, preformatted=false) {

    var ediv = '<div class="alert alert-info" role="alert">'+ msg + '</div>';
    var html = preformatted ? '<pre>'+ ediv + '</pre>' : ediv; 
    $("#result-info").append(html);
    
  }

  self.setTxtDowloadLink = function( text='' ) {

    var blob = new Blob([text], {type: 'text/plain'});
    $("#download-link").on("click", function () {
      saveAs(blob, 'knot-finder-results.txt');
    });

  };

  self.setZipDownloadLink = function( data ) {

    var zip = new JSZip();
    zip.file("output.txt", data.result.out);
    zip.file("err.txt", data.result.err);
    zip.file("log.txt", data.result.log);

    $("#download-link").on("click", function () {
      
      zip.generateAsync( {type: "blob"} ).then( function ( blob ) { // 1) generate the zip file
  
          saveAs(blob, "knot-finder-results.zip");            // 2) trigger the download
  
      }, function (err) {

          self.errorMessage(err, true);
      
      });
    
    });

  };


  self.setLogs = function( data ) {

    var parmtxt = JSON.stringify( data.param, null, 2 );
    $('#log-prm').html( parmtxt );
    $('#log-cmd').html( data.cmd );
    $('#log-out').html( data.result.out );
    $('#log-err').html( data.result.err );
  
  }


  self.abortButton = function( action ) {
    
    if ( action === 'show' ){

      $('#abort-job-btn').show();

    } else if ( action === 'hide' ) {

      $('#abort-job-btn').hide();      

    }

  }

  self.getRunOptions = function() {
    
    var options = {
     
      full : $('#full-yes').prop('checked'),
      topdown : $('#topdown').prop('checked'),
      stride: $('#stride-edt').val(),
    
    }
    return options;

  }

  self.showKnotDescription = function( status ) {

    self.clearMessageBoard();

    var i = status.getCurrentIndex();
    var kt = status.knot_type[ i ];
    var txt = '';
    if ( kt === false ) {
      
      txt = '';
    
    } else if ( kt === "UN" ) {

      txt = 'No knots detected';
    
    } else {

       
      txt = 'Alexander polynomial evaluated at t=-1 and t=-2: <p class="mathtext" style="padding-left:1em;">A(-1)=' + status.alexander_dets[ i ][ 0 ] + '<br>A(-2)=' + status.alexander_dets[ i ][ 1 ] + '</p>';
      txt += '<div>Simplest knot type compatible with A(-1) and A(-2):<p>'
      if ('kt' == '??') {
        txt += '<span class="mathtext"><mark>??</mark></strong>';
      } else {
        cross_ord = kt.split('_');
        if (cross_ord[1] === undefined) {
          txt += '<span class="mathtext"><mark><strong>' + cross_ord[ 0 ] + '</strong></mark></span>';
        } else {
          txt += '<span class="mathtext"><mark><strong>' + cross_ord[ 0 ] + '<sub>' + cross_ord[ 1 ] + '</sub></strong></mark></span>';
          txt += '<small><a target="_blank" href="http://katlas.org/wiki/' + kt + '">[view on KnotAtlas]</a></small><br/>';
        }
      }
      txt += '</p></div>';
      var range = status.getKnotRange( i );
      var start = range[ 0 ], end = range[ 1 ];
      if ( status.hasPdbInfo() ) {

        var e0 = status.getCurrentChainObject()[ range[ 0 ] ];
        var e1 = status.getCurrentChainObject()[ range[ 1 ] ];
        start = e0.resName + ':' + e0.resSeq + ' (' + range[ 0 ] + ')';
        end = e1.resName + ':' + e1.resSeq + ' (' + range[ 1 ] + ')';

      }
      txt += 'Position: <p class="mathtext">' + start + ' &#8594; ' + end + '</p>';

    }

    $('#viewer-info').html(txt);

    if ( txt !== '' ) 
      self.infoMessage(txt);
    
  }

  self.drawKymo = function( status ) {

    $('.kymo').remove();
    
    var kymodata = status.trajectory_flag ? status.kymo[ 0 ] : status.kymo[ status.getCurrentIndex() ];
    if ( kymodata ) {
      
      var kymo = $('<img class="kymo" width=100% heigth=20% src=\'data:image/svg+xml;utf8,' + kymodata + '\'>');
      $('#kymo-div').append(kymo);

    }
    
  }

}
