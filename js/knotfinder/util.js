// deepcopy an object
function deepcopy(obj){
  return JSON.parse(JSON.stringify(obj));
}

// python style range
function range(start, end, increment) {
    var array = [];
    var current = start;

    increment = increment || 1;
    if (increment > 0) {
        while (current < end) {
            array.push(current);
            current += increment;
        }
    } else {
        while (current > end) {
            array.push(current);
            current += increment;
        }
    }
    return array;
}

// close-to-unique-uids
function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
}


// check proximity of 3d points
function allClose(a1, a2, tol=0.0001){
  return a1.length==a2.length && a1.every(function(v,i) { return Math.abs(v - a2[i]) < tol})
}


// check if coordinates are a ring
function isRing(coordinates){
  return allClose(coordinates[0], coordinates[coordinates.length - 1]);
}


// utilities for overlapping ranges
function RangeEvent(pos, id, etype){
  this.x = pos;
  this.id = id;
  this.start = ( etype === 'start' );
  this.stop = !this.start; 
}

function stackUniqueRanges(ranges, n) { 
  // superimpose in order of importance input ranges

  // populate events
  var events = [];
  for (var i = 0; i < ranges.length; i++) {
  
    var start = ranges[i][0];
    var stop = ranges[i][1];
    events.push( new RangeEvent(start, i, 'start') );
    events.push( new RangeEvent(stop, i, 'stop') );
  
    if ( start > stop ){
    
      events.push( new RangeEvent(0, i, 'start') );
      events.push( new RangeEvent(n, i, 'stop') );

    }
    
  }

  // sort events by position and then by id
  // smaller id are prioritized
  // start goes before end
  events.sort(function(a, b) {
  
    return a.x - b.x || a.id - b.id || (a.start ? -1 : 1);
    
  });


  var currentStatus = Infinity;
  var rangeStart = false;
  var newRanges = [];
  var statusStack = [];


  console.log(currentStatus, rangeStart, statusStack);

  for (var i = 0; i < events.length; i++) {
    
    var e = events[i];
    if( e.start ) {

      statusStack.push( e.id );
      statusStack.sort();
      if ( e.id < currentStatus ) {

        if (rangeStart !== false && rangeStart !== e.x ) {

          newRanges.push( [currentStatus, rangeStart, e.x] );

        }

        currentStatus = e.id;
        rangeStart = e.x;

      } 

    }
    if ( e.stop ){

      if ( e.id === currentStatus) {

        if ( rangeStart !== e.x )
          newRanges.push( [e.id, rangeStart, e.x] );
        statusStack.splice(0, 1);
        if (statusStack.length > 0){

          rangeStart = e.x;
          currentStatus = statusStack[0];
        
        } else {
        
          rangeStart = false;
          currentStatus = Infinity;
        
        }

      } else {

        var idx = statusStack.indexOf(e.id);
        statusStack.splice(idx, 1);

      }

    }

  }

  return newRanges;

}

function stackRanges(ids, ranges, n) { 
  // stack ranges

  // populate events
  var events = [];
  for (var i = 0; i < ranges.length; i++) {
  
    var start = ranges[i][0];
    var stop = ranges[i][1];
    events.push( new RangeEvent(start, ids[i], 'start') );
    events.push( new RangeEvent(stop, ids[i], 'stop') );
  
    if ( start > stop ){
    
      events.push( new RangeEvent(0, ids[i], 'start') );
      events.push( new RangeEvent(n, ids[i], 'stop') );

    }
    
  }

  // sort events by position and then by id
  // smaller id are prioritized
  // start goes before end
  events.sort(function(a, b) {
  
    return a.x - b.x || a.id - b.id || (a.start ? -1 : 1);
    
  });


  var rangeStart = false;
  var newRanges = [];
  var statusStack = [];

  for (var i = 0; i < events.length; i++) {
    var e = events[i];

    if( e.start ) {

      if ( rangeStart !== false && rangeStart !== e.x ) {

        newRanges.push( [statusStack.slice(), rangeStart, e.x] );

      }

      rangeStart = e.x;
      statusStack.push( e.id ); 

    }

    if ( e.stop ){

      if ( rangeStart !== false && rangeStart !== e.x )
        newRanges.push( [statusStack.slice(), rangeStart, e.x] );

      var idx = statusStack.indexOf(e.id);
      statusStack.splice(idx, 1);

      if ( statusStack.length > 0 ){

        rangeStart = e.x;
        
      } else {
      
        rangeStart = false;
      
      }

    }


    // console.log(i, 'pos', e.x, 'id', e.id, 'type', (e.start ? 'start' : 'stop'));
    // console.log('currentStatus', currentStatus, 'rangeStart', rangeStart, 'stack', statusStack);

  }

  return newRanges;

}
