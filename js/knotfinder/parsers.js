
function parsePDB( text , onlyCA=false ) {

  var lines = text.split( '\n' );
  var chains = [];
  chains.chainNames = [];
  var last_chain = null;
  var curr_chain = [];
  curr_chain.coordinates = [];
  curr_chain.resSeqs = [];
  curr_chain.resNames = [];
  curr_chain.names = [];

  for ( var i = 0, l = lines.length; i < l; i ++ ) {

    if ( lines[ i ].substr( 0, 4 ) === 'ATOM' || lines[ i ].substr( 0, 6 ) === 'HETATM' ) {

      var name = lines[ i ].substring( 12, 16 ).trim();
      if (onlyCA && name !== "CA") continue;

      x = parseFloat( lines[ i ].substring( 30, 38 ) );
      y = parseFloat( lines[ i ].substring( 38, 46 ) );
      z = parseFloat( lines[ i ].substring( 46, 54 ) );

      var resSeq = parseInt( lines[ i ].substring( 22, 26 ) );
      var resName = lines[ i ].substring( 17, 20 ).trim();
      var chain = lines[ i ].substring( 21, 22 );

      if ( chain !== last_chain ){
        if (curr_chain && curr_chain.length > 0){
          chains.push( curr_chain );
          chains.chainNames.push( curr_chain.chainName );
        }
        curr_chain = [];
        curr_chain.chainName = chain; 
        curr_chain.coordinates = [];
        curr_chain.resSeqs = [];
        curr_chain.resNames = [];
        curr_chain.names = [];
        last_chain = chain;
      }

      curr_chain.push({
        name: name,
        resSeq: resSeq,
        resName: resName,
        pos: [x, y, z],
      });
      curr_chain.names.push(name);
      curr_chain.coordinates.push([x,y,z]);
      curr_chain.resSeqs.push(resSeq);
      curr_chain.resNames.push(resName);
    }
  }
  if (curr_chain && curr_chain.length > 0){
    chains.push( curr_chain );
    chains.chainNames.push( curr_chain.chainName );
  }
  return chains
}


function parseKNT(fileString){
  
  var lines = fileString.split( "\n" );
  var n_atoms = parseInt( lines[0] );
  
  if ( isNaN( n_atoms ) || n_atoms <= 0 || lines.length < n_atoms + 1 ) {
  
    alert( 'Cannot read NXYZ file. Malformed input at line 1.' );
    return false;
  
  }
  
  var coordinates = [];
  var frameStart = 0;

  while ( frameStart < lines.length ) {

    if ( lines[frameStart].trim() === "" ) {
      
      frameStart++;
      continue;
    
    }

    var n_atoms_2 = parseInt( lines[ frameStart ] );
    
    if ( n_atoms_2 !== n_atoms ) {
    
      alert( 'Cannot read NXYZ file. Malformed input at line ' + ( frameStart + 1 ) );
      return false;
    
    }
  
    var frame = [];

    for ( var i = frameStart; i < frameStart + n_atoms; ++i ) {  
    
      var data = lines[i+1].match(/\S+/g) || [];
    
      if ( data.length < 3 ){
    
        alert( 'Cannot read NXYZ file. Malformed input at line ' + ( i + 2 ) );
        return false;
    
      }
    
      var x = parseFloat( data[0] );
      var y = parseFloat( data[1] );
      var z = parseFloat( data[2] );
    
      if ( isNaN(x) || isNaN(y) || isNaN(z) ) {
    
        alert( 'Cannot read NXYZ file. Malformed input at line ' + ( i + 2 ) );
        return false;
    
      }
    
      frame.push( [ x, y, z ] );
    
    }  

    coordinates.push(frame);
    frameStart += n_atoms + 1;

  }

  return coordinates;

}