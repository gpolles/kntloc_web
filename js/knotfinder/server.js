// $('button#run-job-btn').removeClass('btn-primary')
//                          .addClass('btn-warning')
//                          .html('<i class="fa fa-circle-o-notch fa-spin"></i> Finding knots...');
  
//                           .html('Find Knots')
//                          .removeClass('btn-warning')
//                          .addClass('btn-primary')

function flatten(arr) {
  return arr.reduce(function (flat, toFlatten) {
    return flat.concat(Array.isArray(toFlatten) ? flatten(toFlatten) : toFlatten);
  }, []);
}

var KnotFinderServer = function(callbacks) {

  var self = this;
  var url = 'ajax_run.php';
  var cancelUrl = 'ajax_cancel.php';
  var pollUrl = 'ajax_poll.php';
  var failedAttemptsLimit = 5;
  self.running = false;
  var failedAttempts = 0;
  var pollInterval = 1000;
  var ignoreList = {};
  self.ignoreList = ignoreList;

  self.errorHandler = function(x, err, et){

    self.running = false;
    self.onerror(self, x, err, et);

  }

  self.sendRequest = function(appstatus, callbacks) {

    var internalID = guid();

    self.running = true;
    self.currentRequest = null;
    self.currentRequestInternalID = '' + internalID;

    // set the callbacks
    var callbacks = callbacks || {}
    self.beforestart = callbacks.beforestart || function(){};
    self.sendsuccess = callbacks.sendsuccess || function(){};
    self.onfail = callbacks.fail || function(){};
    self.beforecancel = callbacks.fail || function(){};
    self.remoteError = callbacks.remoteError || function(){};
    self.jobRunning = callbacks.jobRunning || function(){};
    self.queued = callbacks.queued || function(){};
    self.finish = callbacks.finish || function(){};
    self.onupload = callbacks.onupload || function(){};
    self.oncancel = callbacks.cancel || function(){};
    

    self.onerror = callbacks.error || function(){};

    failedAttempts = 0;

    // run preparation
    self.beforestart(self);

    // prepare data
    var runOptions = appstatus.getRunOptions();
    var range = appstatus.getActiveBeadRange();
    var linear = ! appstatus.isRing();
    // if ( !linear && range[1] == appstatus.getCurrentStructure().length - 1 )
    //   range = [range[0], range[1] - 1];

    var num_frames = appstatus.num_frames;
    var coordinates = null;
    if ( appstatus.isTrajectory() ) {

      coordinates = flatten( appstatus.getTrajectory() );
    
    } else {

      coordinates = flatten( appstatus.getCurrentStructure() );
    
    }

    var num_beads = coordinates.length / 3 / num_frames;

    var data = {

      num_frames: num_frames, 
      num_beads: num_beads,
      coordinates: coordinates,
      linear: linear,
      full: runOptions.full,
      topdown: runOptions.topdown,
      stride: runOptions.stride,
      range: range,

    };

    console.log(data);
    var jsondata = { jsondata: JSON.stringify(data) };
    
    // send async request to server
    $.ajax({
      type: "POST",
      url: url,
      data: jsondata,
      success: function( data ) {

        if ( internalID in ignoreList ) return false;
        self.processResults( data, internalID );

      },
      dataType: 'json',
      error: function(x, err, et){

        if ( internalID in ignoreList ) return false;
        self.running = false;
        self.onerror(self, x, err, et);

      },
      // this stuff is to have updatable upload status
      xhr: function () {

        var jqXHR = null;
        
        jqXHR = new window.XMLHttpRequest();
        
        //Upload progress
        jqXHR.upload.addEventListener( "progress", function ( evt )
        {
            if ( internalID in ignoreList ) return false;
            if ( evt.lengthComputable )
            {
              self.onupload(evt.loaded, evt.total);
            }
        }, false );

        return jqXHR;

      },

    });

  }


  self.stopRequest = function(){
    
    if (! self.running ) return false;
    ignoreList[ self.currentRequestInternalID ] = 1;
    
    $.ajax({
      type: "POST",
      url: cancelUrl,
      data: {

        id: self.currentRequest,
        
      },
      success: function(data){

        console.log(data);
        self.running = false;
        self.oncancel(self);

      },
      dataType: 'json',
      error: function(x, err, et){

        // even if something goes wrong, we want the user to regain 
        // control of the interface. Just sweep underneath the rug.
        console.log('got error on cancel', x, err, et);
        self.running = false;
        self.oncancel(self);

        //self.onerror(self, x, err, et);

      },
    });

  }


  self.processResults = function(data, internalID){

    if ( internalID in ignoreList ) return false;

    console.log(data);
    
    if (data.status == 'ok'){ 

      self.currentRequest = data.data;
      self.sendsuccess(self);
      setTimeout(self.pollStatus( internalID ), pollInterval);

    } else {

      self.onfail(self, 'Server error', data);

    }
    
  }



  self.pollStatus = function( internalID ){

    function inner(){

      if ( internalID in ignoreList ) return false;
    
      if ( ! self.running ) return false;

      jsondata = { id: self.currentRequest };

      $.ajax({
        type: "POST",
        url: pollUrl,
        data: jsondata,
        success: self.gotStatus( internalID ),
        dataType: 'json',
        error: function(x, err, et){

          if ( internalID in ignoreList ) return false;

          self.running = false;
          if ( failedAttempts < failedAttemptsLimit ) {

            console.log(`poll request failed. Retrying (${failedAttempts}/${failedAttemptsLimit})`);
            failedAttempts++;
            setTimeout(self.pollStatus( internalID ), pollInterval);
          
          } else {
           
            self.onerror(self, x, err, et);
          
          }

        },

      });

    }

    return inner;

  }

  self.gotStatus = function ( internalID ){

    var inner = function( data ){

      if ( internalID in ignoreList ) return false;

      failedAttempts = 0;

      if ( data.status === 'ok' ) { 

        self.jobstatus = data.data;

        if ( self.jobstatus.status === 'running' ){

          self.jobRunning(self);
          setTimeout(self.pollStatus( internalID ), pollInterval);

        }

        if ( self.jobstatus.status === 'queued' ){

          self.queued(self);
          setTimeout(self.pollStatus( internalID ), pollInterval);

        }

        if ( self.jobstatus.status === 'completed' ){

          self.running = false;
          self.finish(self);

        }

        if ( self.jobstatus.status === 'error' ){


          self.remoteError(self);

        }

      } else {

        self.jobstatus = data;
        self.remoteError(self);

      }

    };

    return inner;

  } 


}
