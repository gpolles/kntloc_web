
var webgl_error_message =`
  <p>
    Error. I was unable to create the WebGL window. <br/>
  </p>
  <p>
    Check this page to see if webgl is supported on your browser:
    <a href="https://get.webgl.org/">https://get.webgl.org/</a>.
  </p>
  <p><small><i>
    If you are using Google Chrome on a Mac, it may be because of your
    hardware setup. More details should be available on 
    <a href="chrome://gpu/">chrome://gpu/</a>
    You may try to run chrome manually specifying --ignore-gpu-blacklist,
    but you'll probably just want to use Safari for this website.    
  </i></small></p>`

var TubeView = function(container, options){

  var self = this;

  var container, stats;

  var camera, controls, scene, renderer;

  var cross;

  var container = container;

  var options = options || {};
  var backgroundColor = options.color || 0xeeeeee;

  var tubeOptions = options.tubeOptions || {

    splineQuality: 10,
    tubeWidth: 0.2,

  };

  var splineOptions = options.splineOptions || {

    curveType: 'catmull',

  };

  self.materials = {
    transparent: new THREE.MeshLambertMaterial({ 
      color: 0x2e9afe, 
      opacity: 0.2,
      transparent: true,
    }),

    solid: new THREE.MeshLambertMaterial({ 
      color: 0x2e9afe, 
      side: THREE.DoubleSide, 
      transparent: false, 
      reflectivity: 0,
      envMap: null,
    }),

    highlight: new THREE.MeshLambertMaterial({ 
      color: 0xfe9a2e, 
      side: THREE.DoubleSide, 
      transparent: false, 
      reflectivity: 0,
      envMap: null,
    }),

    transparent_highlight: new THREE.MeshLambertMaterial({ 
      color: 0xfe9a2e, 
      opacity: 0.2,
      transparent: true,
    }),

  };

  
  camera = new THREE.PerspectiveCamera( 50, container.offsetWidth / container.offsetHeight, 1, 10000 );
  camera.position.z = 50;

  self.group = new THREE.Group();

   // lights

  var light = new THREE.DirectionalLight( 0xffffff, 0.7);
  light.position.set( 100, 0, 50);
  camera.add( light );
  var light = new THREE.DirectionalLight( 0xffffff, 0.5);
  light.position.set( -50, -50, -50);
  camera.add( light );

  var ambient_light = new THREE.AmbientLight(0xffffff, 0.3);

  //var light = new THREE.DirectionalLight( 0x002288 );
  //light.position.set( -1, -1, -1 );
  //scene.add( light );

  //var light = new THREE.AmbientLight( 0x222222 );
  //scene.add( light );

  controls = new THREE.TrackballControls( camera, container );

  controls.rotateSpeed = 5.0;
  controls.zoomSpeed = 10.2;
  controls.panSpeed = 0.8;

  controls.noZoom = false;
  controls.noPan = false;

  controls.staticMoving = true;
  controls.dynamicDampingFactor = 0.3;

  controls.keys = [ 65, 83, 68 ];


    // world

  scene = new THREE.Scene();
  scene.background = new THREE.Color( backgroundColor );
  scene.add(camera);
  scene.add(ambient_light);
  scene.add(self.group);
  //scene.fog = new THREE.FogExp2( 0xcccccc, 0.002 );

  // renderer

  try {

    renderer = new THREE.WebGLRenderer( { antialias: true } );  
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize( container.offsetWidth, container.offsetHeight, false );
    container.appendChild( renderer.domElement );

  } catch (error) {

    $( container ).html(webgl_error_message);
    console.log( error );
  
  }
  

  function getAverageSpacing(points){
  
    var avdist = 0.0;
    for (var i = 0; i < points.length-1; ++i){

      avdist += points[i].distanceTo(points[i+1]);

    }
    return avdist/(points.length-1);
  
  }

  function crdToVec3s(coordinates){
    points = [];
    for (var i = 0; i < coordinates.length; ++i){

      var pos = coordinates[i];
      points.push(new THREE.Vector3( pos[0], pos[1], pos[2] ));

    }
    return points;
  }

  self.getCenter = function(points){

    var com = new THREE.Vector3(0, 0, 0);
    for (var i = 0; i < points.length; ++i)
      com.add(points[i]);
    return com.multiplyScalar(1.0/points.length);

  }


  self.onWindowResize = function() {

    camera.aspect = container.offsetWidth / container.offsetHeight;
    camera.updateProjectionMatrix();

    renderer.setSize( container.offsetWidth, container.offsetHeight );

    controls.handleResize();

    self.render();

  }

  self.animate = function() {

    requestAnimationFrame( self.animate );
    controls.update();

  }

  self.render = function() {
    
    if (renderer)
      renderer.render( scene, camera );

  }

  controls.addEventListener( 'change', self.render );
  window.addEventListener( 'resize', self.onWindowResize, false );


  self.render();
  self.animate();

  self.getSpline = function(coordinates, closed=false, curve_type='chordal', tension=0.5, splineFactor=3){
    
    var vecs = crdToVec3s(coordinates);
    var avedist = getAverageSpacing(vecs);
    var p = closed ? 0: 1;
    var numSplinePoints = (coordinates.length - p)*splineFactor;
    var splineWidth = avedist/16;

    var pipeSpline = new THREE.CatmullRomCurve3( vecs, closed, curve_type, tension );
    return pipeSpline

  }

  self.PartialSpline = function ( spline, start, end, length=1.0, scale=1.0) {

    THREE.Curve.call( this );
    this.scale = ( scale === undefined ) ? 1 : scale;
    this.spline = spline;
    this.start = start / length;
    this.end = end / length;
    this.range = this.end - this.start;

  }

  self.PartialSpline.prototype = Object.create( THREE.Curve.prototype );
  self.PartialSpline.prototype.constructor = self.PartialSpline;

  self.PartialSpline.prototype.getPoint = function ( t ) {
    
    return this.spline.getPoint( this.start + t*this.range ).multiplyScalar( this.scale );

  };

  self.scaleCoordinates = function(points, maxSpan){

    var bbox = new THREE.Box3().setFromPoints(points);
    var sizes = bbox.getSize();
    var maxDim = Math.max(sizes.x, sizes.y, sizes.z);
    var scale = maxSpan/maxDim;
    for (var i = 0; i < points.length; ++i){
      points[i].multiplyScalar(scale);
      
    }
  }

  self.setStructure = function(coordinates, opts){
    
    var opts = opts || {};
    self.closed = opts.closed || false;
    self.curveType = opts.curveType || 'chordal';
    self.tension = opts.tension || 0.5;
    
    // transform coordinates into points, scale and center them
    self.points = crdToVec3s(coordinates);
    self.numPoints = coordinates.length - (self.closed ? 0: 1);
    self.scaleCoordinates(self.points, 16);
    var center = self.getCenter(self.points);
    for (var i = 0; i < points.length; ++i)
      self.points[i].sub(center);

    self.pipeSpline = new THREE.CatmullRomCurve3( self.points, self.closed, self.curve_type, self.tension );

  }

  self.addTube = function(opts){

    var opts = opts || {};
    var tubeWidth = opts.tubeWidth || 0.1;
    var splineQuality = opts.splineQuality || 1;
    var materialName = opts.material || 'solid';
    var range = opts.range || [0, self.numPoints];
    var splineWidth = opts.splineWidth || 1;
    var tubeQuality = opts.tubeQuality || 10;

    if ( range[0] > self.numPoints || range[1] > self.numPoints){
      console.log('invalid range?', range, 'numPoints:', self.numPoints);
    }
    if ( range[0] > range[1] ){
      var x1 = range[1];
      var x2 = range[0];
      opts.range = [0, x1];
      self.addTube(opts);
      opts.range = [x2, self.numPoints];
      self.addTube(opts);
      return false;
    }

    // get average distances to use for the tube width
    var avedist = getAverageSpacing(self.points);
    var currTube = new self.PartialSpline(self.pipeSpline, range[0], range[1], self.numPoints);
    var currNumPoints = Math.abs(range[1] - range[0]);

    var numSplinePoints = currNumPoints * splineQuality;
    var geometry = new THREE.TubeBufferGeometry( currTube, numSplinePoints, tubeWidth*avedist, tubeQuality, false );
    var mesh = new THREE.Mesh( geometry, self.materials[materialName] );
    self.group.add(mesh);

  }

  self.clear = function(render=true){

    for ( var i=self.group.children.length -1 ; i >= 0; --i ){
      obj = self.group.children[i];
      self.group.remove(obj);
    }
    if ( render )
      self.render();

  }

  self.redraw = function(status, updateStructure=false) {

    self.clear(false); 
    if (updateStructure){
      
      var crd = status.getCurrentStructure();
      splineOptions.closed = isRing(crd);
      self.setStructure(crd, splineOptions);
      
    }
      
    if ( ! status.num_beads ) {

      console.log('No beads to draw');
      return false;

    }
    // stack annotations
    var activeRange = status.getActiveBeadRange();
    var knotRange = status.getKnotRange();
    if ( knotRange === false ) {
      // no knot detected, set to [0, 0]: will not be drawn.
      knotRange = [0, 0];
    }
    var drawRanges = stackRanges(['knot', 'active', 'all'], [knotRange, activeRange, [0, status.num_beads-1]], status.num_beads-1);

    if (self.points){

      for (var i = 0; i < drawRanges.length; i++) {
        var keys = drawRanges[i][0];
        var start = drawRanges[i][1];
        var stop = drawRanges[i][2];
        var opts = deepcopy(tubeOptions);
        opts.range = [start, stop];
        var knot = keys.indexOf('knot') !== -1;
        var active = keys.indexOf('active') !== -1;
      
        // select material  
        if (knot && active){
          opts.material = 'highlight';  
        } else if (knot && !active) {
          opts.material = 'transparent_highlight';
        } else if (!knot && active) {
          opts.material = 'solid';
        } else if (!knot && !active) {
          opts.material = 'transparent';
        }
      
        self.addTube(opts);  
        
      }
      
      
    }
    
    self.render();

  }


  this.scene = function(){return scene;}
  this.renderer = function(){return renderer;}
  this.camera = function(){return camera;}
  this.controls = function(){return controls;}

}
