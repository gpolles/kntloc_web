<?php 
require('header.php');
?>

  <!-- <script type='text/javascript' src='static/pv/bio-pv.min.js'></script> -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/10.6.0/bootstrap-slider.min.js" integrity="sha256-G5ZTzV4wJSLf9AuZ3wMEJVZDp1MumYGrEw+JsAGHRXI=" crossorigin="anonymous"></script>
  <!-- <script src="js/libs/bootstrap-slider.min.js"></script> -->
  <!-- <script src="js/libs/jszip.min.js"></script> -->
  <!-- <script src="js/libs/FileSaver.min.js"></script> -->
  <script src="https://fastcdn.org/FileSaver.js/1.1.20151003/FileSaver.min.js"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/three.js/90/three.min.js" integrity="sha256-i98YWSjrVSmDzUsq+STtuoKpzRvb3gyWj9Bu5InaJSk=" crossorigin="anonymous"></script>
  <!-- <script src="js/libs/three.min.js"></script> -->
  <!-- <script src="js/libs/TrackballControls.js"></script>


  <script src="js/knotfinder/util.js"></script>
  <script src="js/knotfinder/parsers.js"></script>
  <script src="js/knotfinder/knotfileloader.js"></script>
  <script src="js/knotfinder/appstatus.js"></script>
  <script src="js/knotfinder/interface.js"></script>
  <script src="js/knotfinder/server.js"></script>
  <script src="js/knotfinder/viewer.js"></script>
  <script src="js/knotfinder/knotfinderapp.js"></script>


  <script src="js/main.js"></script> -->
  <script src="js/kymoknot-bundle.js"></script>

  <!-- <link href="css/bootstrap-slider.min.css" rel="stylesheet"> -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/10.6.0/css/bootstrap-slider.min.css" integrity="sha256-D25WHQReXOtVfJZu6lFdpu3ZN4kvVA7/+Af5mPjYlbw=" crossorigin="anonymous" />
  <link href="css/font-awesome.min.css" rel="stylesheet">

  

  <div class="row">
    <div class="col">
      <ul class="nav">
        <!--
        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Actions</a>
        <div class="dropdown-menu">
          <a class="dropdown-item" href="#" data-toggle="modal" data-target="#upload-dialog">
            <i class="fa fa-folder-open text-success" aria-hidden="true"></i> Load a coordinate file</a>
          <a class="dropdown-item" href="#"><i class="fa fa-cloud-download text-info" aria-hidden="true"></i> Download results</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#"><i class="fa fa-file-text text-warning" aria-hidden="true"></i> Show logs</a>
        </div>
        -->
        <li class="nav-item">
          <a class="nav-link toolbar-link load-link" href="#" data-toggle="modal" data-target="#upload-dialog">
            <i class="fa fa-folder-open text-success" aria-hidden="true"></i> Load a coordinate file
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link toolbar-link disabled" href="#" id="download-link">
            <i class="fa fa-cloud-download text-info" aria-hidden="true"></i> Download results
          </a>
        </li>
<!--
        <li class="nav-item">
          <a class="nav-link toolbar-link disabled" href="#" data-toggle="modal" data-target="#log-dialog">
            <i class="fa fa-file-text text-warning" aria-hidden="true"></i> Show logs
          </a>
        </li>
-->
      </ul>
    </div>
  </div>

  <div class="row">  

    <div class="col col-12 col-md-8 text-center">
      <div id="vv-load-screen" style="position:absolute; top:0; left:0; width:100%; height:100%; background-color: rgba(0,0,0,.4); z-index: 100; display: none;">
        <span style="position:absolute; top:40%;left:40%; font-size:30px;">
            <i class="fa fa-circle-o-notch fa-spin"></i> Loading...
        </span>
      </div>
      <div class="row">
        <div class="col">
          <div id="viewer" style="width:100%;"></div>
          <div id="viewer-info"></div>
        </div>
      </div>
      <div id="viewer-help">
        <a id="viewer-help-tooltip" href="#" data-toggle="tooltip" data-html="true" data-placement="bottom" title="<p>left button: <strong>rotate</strong><br/>
           rigth button: <strong>move</strong><br/>
           wheel: <strong>zoom</strong></p>">
          Mouse controls
        </a>
      </div>
      <div class="row">
        <div class="col" id="trajectory-controls" style="text-align: center; display: none;">
          <button id="traj-prev" class="btn btn-outline-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i></button>
          <span id="traj-frame" style='margin-right: 2em; margin-left: 2em;'></span>
          <button id="traj-next" class="btn btn-outline-secondary"><i class="fa fa-chevron-right" aria-hidden="true"></i></button>
        </div>
      </div>
      <div class="row">
        <div class="col">
          <div id="kymo-div"></div>
        </div>
      </div>

    </div>

    <div class="col-12 col-md-4 invisible" id="control-window">
      <div class="row">
        <div class="col col-sm">
          <form id="jobform" action="submitjob.php" method="post" enctype="multipart/form-data">
            <div class="row mb-4" id="chain-selector-form" style="display: none">
              <div class="col col-4">    
                Chain
              </div>
              <div class="col col-8">
                <select class="form-control" id="chain-selector"></select>
              </div>            
            </div>

          <div class="row">
            <div class="col"> 
              <button type="button" name="run-job-btn" id="run-job-btn" class="btn btn-primary" 
                data-loading-text='<i class="fa fa-circle-o-notch fa-spin"></i> Computing Knot Positions' disabled>Find Knots</button>
              <button type="button" name="abort-job-btn" id="abort-job-btn" class="btn btn-danger" style="display: none;">Abort job</button>
              
              <a class="btn btn-outline-primary" data-toggle="collapse" href="#run-options" role="button" aria-expanded="true" aria-controls="run-options">
                <i class="fa fa-chevron-right" aria-hidden="true"></i> Run options
              </a>
            </div>
          </div> 

          <div id="run-options" class="collapse">
            <div class="card card-body">
              
              <div class="form-group">

                <label>    
                  Approach
                </label>
                
                <div class="form-row">

                  <div class="col">
                    <div class="form-check form-check-inline">
                      <label class="form-check-label">
                        <input type="radio" name="bottomup" id="bottomup" class="form-check-input" value="yes" checked>
                        Bottom-Up
                      </label>
                    </div>
                  </div>
                  
                  <div class="col">
                    <div class="form-check form-check-inline">
                      <label class="form-check-label">
                        <input type="radio" name="bottomup" id="topdown" class="form-check-input" value="no">
                        Top-Down
                      </label>
                    </div>
                  </div>
                </div>
              </div>

              <div class="form-group" style="display: none;">

                <label>Type of run</label>    

                <div class="form-row">

                  <div class="col">
                    <div class="form-check form-check-inline">
                      <label class="form-check-label">       
                        <input type="radio" name="full" value="yes" id="full-yes" class="form-check-input" checked>
                        Full
                      </label>
                    </div>
                  </div>
                  <div class="col">
                    <div class="form-check form-check-inline">
                      <label class="form-check-label">
                        <input type="radio" name="full" value="no" class="form-check-input" id="full-no">
                        Quick
                      </label>
                    </div>
                  </div>
                </div>
              </div>

              <div class="form-group" id="ring-controls">
                <label>    
                  Polymer type
                </label>

                <div class="form-row">

                  <div class="col">    
                    <div class="form-check form-check-inline">
                      <label class="form-check-label">       
                        <input type="radio" name="ring" value="linear" id="ring-no" class="form-check-input" checked>
                        <span>Linear</span>
                      </label>
                    </div>
                  </div>

                  <div class="col">    
                    <div class="form-check form-check-inline">
                      <label class="form-check-label">       
                        <input type="radio" name="ring" value="ring" id="ring-yes" class="form-check-input">
                        Ring
                      </label>
                    </div>
                  </div>

                </div>
              </div>

              <div class="form-group">
                <label>    
                  Bead Range
                </label>

                <div class="form-row">
                  <div class="col"> 
                    <input id="slider-range" type="text" class="span2" value="" data-slider-min="0" data-slider-max="0" data-slider-step="1" data-slider-value="[0,0]"/>
                  </div>
                </div>

                <div class="form-row">
                  <div class="col">
                    <div class="form-group">
                      <label id="bead-interval-label1" for="bead-interval-start">Begin</label> 
                      <input type="number" name="bead-interval-start" id="bead-interval-start" min="0" max="0" class="form-control">
                    </div>
                  </div>
                  <div class="col">
                    <div class="form-group">
                      <label id="bead-interval-label2" for="bead-interval-end">End</label> 
                      <input type="number" name="bead-interval-end" id="bead-interval-end" min="0" max="0" class="form-control">
                    </div>
                  </div>
                </div>

                <div class="form-row" id="bead-interval-pdb-info">
                  <div class="col">
                    <div class="form-group">
                      <input type="text" id="bead-interval-pdb-start" class="form-control" readonly>
                    </div>
                  </div>
                  <div class="col">
                    <div class="form-group">
                      <input type="text" id="bead-interval-pdb-end" class="form-control" readonly>
                    </div>
                  </div>
                </div>

                <div class="form-row" id="bi-invert-control">
                  <div class="col">
                    <div class="form-check">
                      <label class="form-check-label">
                        <input type="checkbox" name="bead-interval-invert" id="bead-interval-invert" class="form-check-input">
                        Invert
                      </label>
                    </div>
                  </div>
                </div>

              </div> <!-- form-group -->

              <div class="form-group">
                <label for="stride-edt">
                  Stride for rectification
                </label>
                
                <div class="form-row">
                  <div class="col">
                    <input type="number" name="stride" id="stride-edt" class="form-control" value="5">
                  </div>
                </div>

              </div>

            </div> <!-- card -->
          </div> <!-- collapse -->

            <div id="submission_status">
                
            </div>
          </form>
        </div>
      </div>
      <hr>
      <div class="row">
        <div class="col">
          <div id="result-info">&nbsp;</div>
        </div>
      </div>
    </div>
  </div>


  <div class="modal fade" id="upload-dialog" tabindex="-1" role="dialog" aria-labelledby="upload-dialog-label" aria-hidden="true">
    <form id="upload-form">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="upload-dialog-label"><i class="fa fa-folder-open" aria-hidden="true"></i>  Load a coordinate file</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">  
            <div class="form-group">
                  <label for="file-field">Select either a PDB file (only CA atoms will be considered) or a ascii NXYZ file (<a href="#" data-toggle="modal" data-target="#nxyz-details">details</a>)</label>
                  <input type="file" name="input_file" class="form-control" id="file-field">
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-outline-success" id="select-file-btn">Open</button>
          </div>
        </div>
      </div>
    </form>
  </div>

  <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="log-dialog-label" aria-hidden="true" id="log-dialog">
    <div class="modal-dialog modal-lg">
      
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="log-dialog-label">Logs</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <h5>Parameters</h5>
          <div class="alert alert-warning" id="log-prm" role="alert"></div>
          <h5>Command</h5>
          <div class="alert alert-warning" id="log-cmd" role="alert"></div>
          <h5>Output</h5>
          <div class="alert alert-info" id="log-out" role="alert"></div>
          <h5>Standard Error</h5>
          <div class="alert alert-danger" id="log-err" role="alert"></div>
        </div>
      </div>
    </div>
  </div>


  <div id="nxyz-details" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">NXYZ input file</h5>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <p>A NXYZ file is an ASCII text file. The first line is an integer with the number of coordinates N, followed by N lines with 3 decimal numbers representing the cartesian coordinates x, y, z of each point. Multiple frames can be concatenated.</p>
          <p> Example: </p>
          <pre>
          4
          0.54 2.24 -1.12
          0.59 2.63 -1.03
          0.56 2.78 -0.92
          0.51 2.68 -0.79
          </pre>
          <p>For closed rings, the first and last points should have the same coordinates.</p>
        </div>
      </div>

    </div>
  </div>

