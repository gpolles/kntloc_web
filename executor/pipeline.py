#!/home/apache/miniconda2/bin/python

# to be called as:
# cmd OPTIONS_FILE FILE

from subprocess import Popen, PIPE
import argparse
import json
import sys, traceback
import numpy as np 

import matplotlib
if matplotlib.get_backend() != 'SVG':
    matplotlib.use('SVG')
import matplotlib.pyplot as plt


from json import JSONEncoder
class BasicEncoder(JSONEncoder):
    def default(self, o):
        return o.__dict__ 


DEBUG = True 

# GLOBAL PARAMETERS
PROGRAMDIR = "/var/www/html/kymoknot/cgi-bin/"
KNTCLOSE = PROGRAMDIR + "KNTclose.x"
KNTSIMP = PROGRAMDIR + "KNTring_simplify.x"
# KNTLOCRING = PROGRAMDIR + "KNTloc_ring.x"
# KNTLOCLIN = PROGRAMDIR + "KNTloc_linearB.x"
KNTLOCRING = PROGRAMDIR + "KymoKnot_ring.x"
KNTLOCLIN = PROGRAMDIR + "KymoKnot_linear.x"
ENTANG = PROGRAMDIR + "entang.sh"
DEFAULT_STATUS_FILE = 'pipeline.status'

# STEPS
class Steps:
    START = 0
    PARAMETERS_CHECK = 1
    INPUT_CONSISTENCY = 2
    RING_CLOSURE = 3
    KNOT_SIMPLIFICATION = 4
    ENTANG = 5
    SEARCH = 6
    KNOT_CONSISTENCY = 7

# CLASSES

class Knot:
    def __init__(self, kid="---", kid2="---", Adet1=-2, Adet2=-2, s=-2, e=-2, name='UN'):
        self.kid = kid
        self.kid2 = kid
        self.Adet1 = Adet1
        self.Adet2 = Adet2
        self.start = int(s)
        self.end = int(e)
        self.name = name


class Status:
    def __init__(self, RC=1, LS=0, WC="empty output", K=[], running=True):
        self.returncode = RC
        self.lastsuccess = LS
        self.warning = WC
        self.knots = K
        self.running = running

# FUNCTIONS

def dump_status(status, status_file=DEFAULT_STATUS_FILE):
    with open(status_file, 'w') as f:
        json.dump(status, f, cls=BasicEncoder)


def run_command(command):
    print command
    proc = Popen(command, stdout=PIPE, stderr=PIPE)
    out, err = proc.communicate()
    return proc.returncode, out, err


def plot_kymo(knots, vmax):
    
    colors = ['#fe9a2e', '#dddddd']
    
    # 0, 2, 4 - knot bottom, mid, high
    # 1, 3, 5 - empty bottom, mid, high

    heights = [np.zeros(len(knots)) for _ in range(6)]
    ind = np.arange(len(knots)) + 1
    
    vertical = len(knots) > 10
    
    if not vertical:
        knots = list(reversed(knots))

    for i, k in enumerate(knots):
        if k.start == -2:
            heights[ 1 ][ i ] = vmax
        elif k.start <= k.end:
            heights[ 1 ][ i ] = k.start
            heights[ 2 ][ i ] = k.end - k.start + 1
            heights[ 5 ][ i ] = vmax - k.end - 1
        else:
            heights[ 0 ][ i ] = k.end + 1
            heights[ 3 ][ i ] = k.start - k.end - 1 
            heights[ 4 ][ i ] = vmax - k.start

    h = min(0.2 * len(knots), 4)
    if vertical:
        h = 2.5
    fig = plt.figure(figsize=(10, h))
    a=fig.gca()
    a.set_frame_on(False)

    extra_opt = {}
    if (vertical):
        plt.ylabel('Bead index')
        plt.xlabel('Frame')
    else:
        if len(knots) == 1:
            a.set_yticks([])
        else:
            tl = ['%d' % i for i in reversed(range(1, len(knots)+1))]
            extra_opt['tick_label'] = tl
            plt.ylabel('Frame')
        plt.xlabel('Bead index')
    #a.set_xticks([]); a.set_yticks([])
    #plt.axis('off')

    for i in range(6):
        color = colors[ i % 2 ]
        bottom = np.zeros(len(knots))
        # sum all the previous heights
        for j in range( (i // 2) * 2 ):
            bottom += heights[ j ] 
        if vertical:
            plt.bar(ind, heights[ i ], color=color, bottom=bottom, width=1.0)
        else:
            plt.barh(ind, heights[ i ], color=color, left=bottom, height=1.0, **extra_opt)

    fig.savefig('kymo.svg', transparent=True, bbox_inches='tight', pad_inches=0)


def check_options(opts):
    '''
    Make sure every option we are using is completely safe
    '''
    try:
        opts.update({
            'linear': bool(opts['linear']),
            'full': bool(opts['full']),
            'topdown': bool(opts['topdown']),
            'stride': int(opts['stride']),
            'start': int(opts['range'][0]),
            'stop': int(opts['range'][1]),
            'nbeads': int(opts['nbeads']),
        })
        if opts['linear']:
            assert(opts['start'] <= opts['stop'])
    except:
        return False;
    return True


def handle_error(step):
    pass


status = Status()
status.lastsuccess = Steps.START
dump_status(status)

try:
    ### Read arguments

    parser = argparse.ArgumentParser(description='find knot pipeline')
    parser.add_argument('options', type=str, 
                        help='json file with options')
    parser.add_argument('input', type=str, 
                        help='input trajectory in NXYZ')

    args = parser.parse_args()

    input_file = args.input

    ### Parameter check

    # this should be safe - https://stackoverflow.com/questions/38813298/is-json-loads-vulnerable-to-arbitrary-code-execution
    opts = json.load(open(args.options, 'r'))

    if not check_options(opts):
        raise RuntimeError('Invalid options')

    status.lastsuccess = Steps.PARAMETERS_CHECK
    dump_status(status)

    # ### Knot closure

    # closed_file = "input.closed"
    
    # if opts['linear']:
    
    #     r, out, err =  run_command([KNTCLOSE, input_file]) 
    
    #     if r != 0:
    #         raise RuntimeError('Chain closure failed: \n' + err)
    
    #     with open(closed_file, 'w') as f:
    #         f.write(out)
    
    # else:
    
    #     shutil.copyfile(input_file, closed_file)

    # status.lastsuccess = Steps.RING_CLOSURE
    # dump_status(status)

    # ### Knot semplification
    
    # simplified_file = "input.simpl"
    # r, out, err =  run_command([KNTSIMP, closed_file])    
    
    # if r != 0:
    #     raise RuntimeError('Chain simplification failed: \n' + err)
    #     with open(closed_file, 'w') as f:
    #         f.write(out)

    # status.lastsuccess = Steps.KNOT_SIMPLIFICATION
    # dump_status(status)
    
    # ### Entang for knot identification

    # entang_file = simplified_file + "_details"
    # r, out, err =  run_command([ENTANG, simplified_file])    
    
    # if r != 0:
    #     raise RuntimeError('Entang knot identification failed: \n' + err)
    
    # with open(entang_file, 'r') as f:
    #     lines = f.readlines()
    #     details = lines[-1].split()
    
    #     entang_knot = Knot(Adet1=int(details[-5]), Adet2=int(details[-4]),
    #                        kid=details[-3], kid2=details[-2])

    # status.knots = [entang_knot]
    # status.lastsuccess = Steps.ENTANG
    # dump_status(status)

    ### Knot localization

    command_options = [ 
        "-s", str(opts['stride']), 
    ] 

    n_beads = int(open(input_file, 'r').readline())
    span = int( np.abs(int(opts['stop']) - int(opts['start'])) ) + 1

    if opts['linear'] or span < n_beads:
        command_options += [
            "-F", str(opts['start']), 
            "-T", str(opts['stop']) 
        ]

    command_options += [ "-t" if opts['topdown'] else "-b" ] 
    prefix = "TD__" if opts['topdown'] else "BU__"
    command = KNTLOCLIN if opts['linear'] else KNTLOCRING 
    localization_file = prefix + input_file
    command_options += ['-m', str(n_beads)]

    if DEBUG:
        with open('debug.txt', 'w') as f:
            f.write(' '. join([command] + command_options + [input_file]))
            f.write('\n')
            f.write(json.dumps(opts))

    r, out, err = run_command([command] + command_options + [input_file])
    
    if r != 0:
        raise RuntimeError('Knot localization failed: \n' + err)
    
    localized_knots = []
    with open(localization_file, 'r') as f:
        for line in f:
            if len(line.strip()) == 0 or line[0] == '#':
                continue
            k_loc = line.split()
            if len(k_loc) < 6:
                raise RuntimeError('Invalid output')
            localized_knots.append(
                Knot(
                    Adet1=k_loc[1], 
                    Adet2=k_loc[2], 
                    name=k_loc[3],
                    s=k_loc[4], 
                    e=k_loc[5], 
                )
            )

    status.knots = localized_knots
    status.lastsuccess = Steps.SEARCH
    status.warning = ''

    plot_kymo(localized_knots, opts['nbeads'])
    dump_status(status)

    ### All right, no errors

    status.returncode = 0
    status.running = False
    status.warning = ''
    dump_status(status)

except Exception, e:

    traceback.print_exc(file=sys.stderr)
    status.running = False
    status.warning = traceback.format_exc()
    dump_status(status)





