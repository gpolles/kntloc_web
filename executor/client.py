#!/home/apache/miniconda2/bin/python

import zmq 
import pickle
import json
import sys
import argparse

DEFAULT_PORT = 48579

class Client(object):

    def __init__(self, timeout=30):

        self._context = zmq.Context()
        self._socket = self._context.socket(zmq.REQ)
        addr = 'tcp://127.0.0.1:' + str(DEFAULT_PORT)
        self._socket.connect(addr)
        self._poller = zmq.Poller()
        self._poller.register(self._socket, zmq.POLLIN)
        self.timeout = timeout

    def recv(self):
        if self._poller.poll(self.timeout*1000): # poll receive milliseconds
            msg = self._socket.recv()
            data = pickle.loads(msg)
        else:
            data = [-2, "Timeout processing request"]
        
        if data is None:
            print 'Exiting!'
            return True

        return { 'rc': data[0], 'data': data[1] }

    def send(self, obj):
        self._socket.send(pickle.dumps(obj))

    def sendjob(self, data):
        self.send(data)
        return self.recv()
    
    def shutdown(self):
        self.send(None)
        return self.recv()

    def close(self):
        self._socket.setsockopt( zmq.LINGER, 0 )
        self._socket.close()
        self._context.term()

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        try:
            self.close()
        except:
            pass
    

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='find knot pipeline')
    parser.add_argument('--cmd', type=str, default='run',
                        help='requested command')
    args = parser.parse_args()

    if args.cmd == 'shutdown':
        with Client() as c:
            c.shutdown()

    else:
        with Client() as c:
            
            obj = json.loads(sys.stdin.read())
            print json.dumps(c.sendjob([args.cmd, obj]))
