#!/home/apache/miniconda2/bin/python

import sys
import traceback
import zmq
from subprocess import Popen, PIPE
import os, signal
#from .lazyio import PopulationCrdFile
from multiprocessing import Process, Queue, Manager
from Queue import Empty
from time import time, sleep
import pickle
from uuid import uuid4
from contextlib import contextmanager
import logging
import shutil
import numpy as np
import json
import urllib

manager = Manager()

logging.basicConfig()

DEFAULT_PORT = 48579
DEFAULT_EXEC_TIME = 3600
TIME_BEFORE_DELETE = DEFAULT_EXEC_TIME*2
MAX_JOBS = 10
NUM_PROCESSES = 2
CLEAN_POOL_TIME = 5
ROOT_DIR = '/var/www/html/kymoknot/'
EXECUTABLE = [
    ROOT_DIR + 'executor/pipeline.py'
]
JOBS_PATH = ROOT_DIR + 'jobs/'

def get_knots(localization_file):
    localized_knots = []
    with open(localization_file, 'r') as f:
        for line in f:
            if line[0] == '#':
                continue
            val = line.split()
            if len(val)<6: 
                break
            localized_knots.append({
                'name' : val[1],
                'Adet1' : val[2],
                'Adet2' : val[3],
                'start' : val[4],
                'end' : val[5]
            })
    return localized_knots

@contextmanager
def cd(newdir):
    prevdir = os.getcwd()
    os.chdir(os.path.expanduser(newdir))
    try:
        yield
    finally:
        os.chdir(prevdir)


class FunctionWrapper(object):
    def __init__(self, inner, const_vars={}, timeout=DEFAULT_EXEC_TIME):
        from functools import partial
        self.inner = partial(inner, **const_vars)
        self.timeout = timeout
        self.pid = None
        self.p = None
        self._manager = Manager()
        self._q = self._manager.Queue()

    def run_inner(self, *args, **kwargs):
        try:
            from time import time
            tstart = time()
            res = self.inner(*args, **kwargs)
            self._q.put((0, res, time() - tstart))
        except:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            tb_str = ''.join(traceback.format_exception(
                exc_type, exc_value, exc_traceback))
            self._q.put((-1, tb_str, None))

    def __call__(self, *args, **kwargs):
        try:
            self.start(*args, **kwargs)
            rval = self.get()
        except:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            tb_str = ''.join(traceback.format_exception(
                exc_type, exc_value, exc_traceback))
            rval = (-1, tb_str, None)
        return rval

    def start(self, *args, **kwargs):
        self._q = Queue()
        self.p = Process(target=self.run_inner, args=args, kwargs=kwargs)
        self.p.start()
        self.pid = self.p.pid
        return self.pid

    def terminate(self):
        if self.p.is_alive():
            self.p.terminate()
            self.p.join()
        self._q.put((-1, 'Terminated by user', None))
    
    def get(self):
        try:
            rval = self._q.get(block=True, timeout=self.timeout)
            self.p.join()
        except Empty:
            rval = (-1, 'Processing time exceeded (%f)' % self.timeout, None)
            self.p.terminate()
            self.p.join()
        except:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            tb_str = ''.join(traceback.format_exception(
                exc_type, exc_value, exc_traceback))
            rval = (-1, tb_str, None)
        return rval


def prepare_job(jid):
    directory = os.path.join(JOBS_PATH, jid)
    os.makedirs(directory)
    with cd(directory):
        with open('status', 'w') as f:
            f.write('queued')


def delete_job(jid, consumers, running_jobs):
    try:
        if jid == '' or jid is None or jid[0] == '.' or jid[0] == '/':
            return {
                'status': 'fail'
            }

        consumer = consumers[ running_jobs.get( jid ) ]
        
        if consumer is not None:
            consumer.kill_job(jid)

        directory = os.path.join(JOBS_PATH, jid)
        
        if os.path.isdir(directory):
            shutil.rmtree(directory)
        
        return {
            'status': 'ok'
        }
    except:
        exc = traceback.format_exc()
        print >>sys.stderr, exc
        return {
            'status': 'fail',
            'reason': exc
        }

def kill_process(pid):
    os.kill(pid, signal.SIGKILL)


def execute_job(jid, data):
    
    directory = os.path.join(JOBS_PATH, jid)
    if not os.path.isdir(directory):
        return 
    
    with cd(directory):

        crds = data.pop('coordinates')
        crds = np.array(crds, dtype=np.float32)
        if len(crds.shape) == 3 and crds.shape[2] == 3:
            n_beads = crds.shape[1]
            data['nbeads'] = n_beads
        elif len(crds.shape) == 1 and 'num_frames' in data and 'num_beads' in data:
            n_beads = data['num_beads']
            data['nbeads'] = n_beads
            crds = crds.reshape((data['num_frames'], n_beads, 3))
        else:
            raise RuntimeError('Invalid coordinates')

        with open('status', 'w') as f:
            f.write('running')
    
        with open('opts.json', 'w') as f:
            json.dump(data, f)

        with open('input_file', 'w') as f:
            for frame in crds:
                f.write('%d\n' % n_beads)
                for xyz in frame:
                    f.write('%f %f %f\n' % (xyz[0], xyz[1], xyz[2]))

        proc = Popen(EXECUTABLE + ['opts.json', 'input_file'], stdout=PIPE, stderr=PIPE)
        out, err = proc.communicate()
        with open('out.txt', 'w') as f:
            f.write(out)
            f.write(err)

        with open('status', 'w') as f:
            f.write('completed')


def check_job(jid):

    directory = os.path.join(JOBS_PATH, jid)

    try:
        with cd(directory):
            with open(os.path.join(directory, 'status')) as f:
                txt = f.read().strip()
            if txt == 'queued':
                return {
                    'status': 'queued'
                }

            if os.path.isfile('pipeline.status'):
                max_trials = 40
                trial = 0
                wait_time = 0.5
                # here we may have some situation in which the file is being 
                # written and the reading fails
                while True:
                    try:
                        data = json.load(open('pipeline.status'))
                        break
                    except:
                        trial += 1
                        sleep(wait_time)
                    if trial > max_trials:
                        raise RuntimeError('pipeline.status exists but it is not a valid json (%s)' % jid)
            else:
            	data = {}

            if txt == 'running':
                opts = json.load(open('opts.json', 'r'))
                prefix = "TD__" if opts['topdown'] else "BU__"
                localization_file = prefix + 'input_file'
                if(os.path.isfile(localization_file)):
                    data['output'] = open(localization_file, 'r').read()
                    data['knots'] = get_knots(localization_file)
            elif txt == 'completed':
                opts = json.load(open('opts.json', 'r'))
                prefix = "TD__" if opts['topdown'] else "BU__"
                localization_file = prefix + 'input_file'
                if os.path.isfile(localization_file):
                    data['output'] = open(localization_file, 'r').read()
                if os.path.isfile('kymo.svg'):
                    data['kymo'] = urllib.quote( open('kymo.svg').read() )

            return {
                'status': txt,
                'data': data,
            }

    except:
        etype, value, tb = sys.exc_info()
        infostr = ''.join(traceback.format_exception(etype, value, tb))

        return {
            'status': 'error',
            'reason': 'file error ' + infostr 
        }


class Consumer(object):

    def __init__(self, job_queue, running_jobs, completed_jobs):
        self.myid = uuid4()
        self.job_queue = job_queue
        self.control_queue = Queue()
        self.running_jobs = running_jobs
        self.completed_jobs = completed_jobs

        self.shared = manager.dict()
        
        self.shared['child_pid'] = -1
        self.shared['worker_pid'] = -1
        self.shared['current_job'] = None

        self.consume_process = Process(target=self.main_loop)
        self.consume_process.start()
        self.running_process = None
        
    def consume_next(self, jobid, data):
        
        try:
            fwrapper = FunctionWrapper(execute_job)
            self.running_jobs[jobid] = self.myid
            self.shared['child_pid'] = fwrapper.start(jobid, data)
            print >> sys.stderr, 'Consumer:', self.myid, 'running job', jobid
            results = fwrapper.get()
            del self.running_jobs[jobid]
            self.completed_jobs[jobid] = (time(), results)
            self.shared['worker_pid'] = -1
            self.shared['child_pid'] = -1
            self.shared['current_job'] = -1
        
        except:
            self.shared['current_job'] = -1
            etype, value, tb = sys.exc_info()
            infostr = ''.join(traceback.format_exception(etype, value, tb))
            print >> sys.stderr, infostr

    def shutdown_children(self):
        if os.path.exists("/proc/%d" % self.shared['worker_pid']):
            try:
                os.kill(self.shared['worker_pid'], signal.SIGTERM)
            except:
                pass
            self.shared['worker_pid'] = -1
        if os.path.exists("/proc/%d" % self.shared['child_pid']):
            try:
                os.kill(self.shared['child_pid'], signal.SIGTERM)
            except:
                pass
            self.shared['child_pid'] = -1
        
    def main_loop(self):
                
        self.running_process = None 
        self.shared['current_job'] = -1

        while True:

            sleep(0.25)

            if self.running_process is not None and not self.running_process.is_alive():
                self.running_process.join()
                self.running_process = None
                self.shared['current_job'] = -1

            if self.shared['current_job'] == -1:

                try:
                    req = self.job_queue.get(timeout=1)    
                    jobid, data = req
                    print >> sys.stderr, 'Consumer:', self.myid, 'accepting job', jobid 
                    self.shared['current_job'] = jobid
                    self.running_process = Process(target=self.consume_next, args=(jobid, data)) 
                    self.running_process.start()
                    self.shared['worker_pid'] = self.running_process.pid
                    
                except Empty:
                    pass
        
    def kill_job(self, jid):
        if jid == self.shared['current_job']:
            del self.running_jobs[jid]
            self.shutdown_children()
            self.shared['current_job'] = -1

    def shutdown(self):
        self.shutdown_children()
        self.consume_process.terminate()
        self.consume_process.join()
    
        
class Server(object):
    def __init__(self, port=DEFAULT_PORT):
        
        self.port = port
        self._context = zmq.Context()
        self._serversocket = self._context.socket(zmq.REP)
        self._serversocket.setsockopt(zmq.SNDTIMEO, 5000)
        self._serversocket.setsockopt(zmq.LINGER, 0)
        addr = 'tcp://*:' + str(self.port)
        self._serversocket.bind(addr)

        self.job_queue = Queue(maxsize=MAX_JOBS)
        self.cleaner_queue = Queue()
        self.consumers_responses = Queue()

        self.running_jobs = manager.dict()
        self.completed_jobs = manager.dict()

        self.consumers = {}
        for i in range(NUM_PROCESSES):
            c = Consumer(self.job_queue,  
                         self.running_jobs, self.completed_jobs)
            self.consumers[ c.myid ] = c
        
        self.cleaner_process = Process(target=self.cleaner)
        self.log_queue = Queue()
        self.log_process = Process(target=self.logger)
        self.rootlogger = logging.getLogger(__name__)
        self.rootlogger.setLevel(logging.DEBUG)
        self.running_processes = manager.dict();

    def log(self, msg, *args, **kwargs):
        
        self.log_queue.put([logging.INFO, msg, args, kwargs])

    def logger(self):
        
        while True:
            
            item = self.log_queue.get()
            
            if item is None:
                break
            
            lvl, msg, args, kwargs = item
            self.rootlogger.log(lvl, msg, *args, **kwargs)

    def loop(self):
        
        try:
            
            self.cleaner_process.start()
            self.log_process.start()
            running = True
            
            while running:
            
                req = self.recv()
                try:

                    if req is None:

                        for cid, consumer in self.consumers:
                            consumer.shutdown()

                        self.cleaner_process.terminate()
                        self.cleaner_process.join()
                        sleep(0.1)
                        self.log_queue.put(None)
                        self.send(None)
                        self.log('Quitting.')
                        break

                    action, data = req

                    if action == 'cancel':

                        self.send([0, delete_job(data['id'], self.consumers, self.running_jobs)]);

                    if action == 'poll':

                        self.send([0, check_job(data['id'])]);

                    elif action == 'run':

                        if self.job_queue.full():

                            self.send([-1, 'Full'])

                        else:

                            jobid = str(uuid4())
                            prepare_job(jobid)
                            self.job_queue.put([jobid, data])
                            self.send([0, jobid])

                except:

                    etype, value, tb = sys.exc_info()
                    infostr = ''.join(
                        traceback.format_exception(etype, value, tb))

                    for _ in range(NUM_PROCESSES):
                        self.job_queue.put(None)

                    self.cleaner_queue.put(None)
                    self.log_queue.put(None)
                    print infostr

        finally:

            self._serversocket.close()
            self._context.term()

            for p in self.processes:
                p.join()

            self.cleaner_process.join()
            self.log_process.join()

    def recv(self):
        rawmessage = self._serversocket.recv()
        return pickle.loads(rawmessage)

    def send(self, obj):
        self._serversocket.send(pickle.dumps(obj))

    def cleaner(self):
        self.log('Cleaner process started')
        while True:
            now = time()
            for jobid, (completion_time, results) in self.completed_jobs.items():
                
                if now - completion_time > TIME_BEFORE_DELETE:
                    delete_job(jobid, self.consumers, self.running_jobs)
                    self.log('deleting job %s' % jobid)
                    del self.completed_jobs[ jobid ]
            sleep(CLEAN_POOL_TIME)
            


s = Server()

try:
    s.loop()

except:
    for cid, consumer in s.consumers:
        consumer.shutdown()
