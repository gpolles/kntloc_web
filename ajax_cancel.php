<?php
$WORKDIR = '/var/www/html/kymoknot';
$executable = $WORKDIR .'/executor/client.py --cmd cancel ';

function prepareResponse( $status, $reason, $data=null ){
  $obj = array(
    "status" => $status,
    "reason" => $reason,
    "data" => $data,
  );
  return json_encode($obj);
}

$JID = $_POST["id"];


$tmpfname = tempnam("jobs/", "tmp");

$handle = fopen($tmpfname, "w");
fwrite($handle, json_encode( array( 'id' => $JID ) ) );
fclose($handle);

$output = array();
$result = 0;
exec('bash -c "cat ' . $tmpfname . ' | ' . $executable . '"', $output, $result);
unlink($tmpfname);

if ( $result !== 0 ) {
  
  die( prepareResponse("fail", "Request failed: ". $executable . $tmpfname ) ); 

}

$out = json_decode( implode( '\n' , $output ), true );

if ( json_last_error() !== JSON_ERROR_NONE ){

  die( prepareResponse("fail", "Invalid output json") );

}

$rc = $out["rc"] === 0 ? "ok" : "fail"; 
die ( prepareResponse( $rc, "", $out[ "data" ] ) );

   
?>
