<?php $DOMAIN = 'http://kymoknot.sissa.it/'; ?>
<!DOCTYPE HTML>
<html lang="en">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> 

<!-- JS -->
<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/popper.min.js"></script> -->
<!-- <script src="js/libs/popper.min.js"></script> --> <!-- needed for tooltips -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<!-- <script src="js/libs/bootstrap.min.js"></script>
 -->
<!-- CSS -->
<!-- <link rel="stylesheet" href="css/bootstrap.min.css"> -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<!--<link rel="stylesheet" href="css/styles.css">-->
<link rel="shortcut icon" type="image/x-icon" href="favicon3.png" />

<style>
  /*@font-face {
    font-family: "Computer Modern";
    src: url('fonts/cmunss.otf');
  }
  @font-face {
    font-family: "Computer Modern";
    src: url('fonts/cmunsx.otf');
    font-weight: bold;
  }
  @font-face {
    font-family: "Computer Modern";
    src: url('fonts/cmunsi.otf');
    font-style: italic, oblique;
  }
  @font-face {
    font-family: "Computer Modern";
    src: url('fonts/cmunbxo.otf');
    font-weight: bold;
    font-style: italic, oblique;
  }*/

  .mathtext {
    font-family: "Times New Roman", serif;
    font-style: oblique;
    padding-left: 1em;
  }

  #viewer-info {
    text-align: left;
    position: absolute;
    top: 0px;
    padding: 5px;
    width: 100%;
    /*font-weight: bold;
    transform-origin: left top;
    zoom: 0.7;
    -moz-transform: scale(0.7);*/
  }

  #viewer-help {
    text-align: left;
    position: absolute;
    top: 0px;
    right:20px;
    padding: 5px;
    text-decoration: none;
    font-size:70%;
  }

  #viewer-help > a,a:hover,a:visited { 
    color:#000; 
    text-decoration: underline;
  }
  
  .tooltip-inner > p {
     text-align:left;
   }

  .slider-selection {
  -webkit-box-shadow: none;
  box-shadow: none; 
  }
</style>

<title>KymoKnot</title>
</head>
<body>

<!-- <div id="load-screen" style="position:absolute; top:0; left:0; width:100vw; height:100vh; background-color: rgba(0,0,0,.4); z-index: 1000;">
    <span style="position:absolute; top:40%;left:40%; font-size:50px;">
        Loading...
    </span>
</div> -->

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#">
    <img src="pics/logo2.png" width="30" height="30" class="d-inline-block align-top" alt="">
    KymoKnot
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a class="nav-link " href=<?php echo "\"$DOMAIN\""; ?>>Home</a>
      </li>
      <li class="nav-item">
        <a class="nav-link " href=<?php echo "\"" . $DOMAIN . "interactive.php\""; ?>>WebServer</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="https://github.com/luca-tubiana/KymoKnot" target="_blank">KymoKnot on GitHub</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href=<?php echo "\"" . $DOMAIN . "contacts.php\""; ?>>Contacts</a>
      </li>
      
    </ul>
  </div>
</nav>
<div class="container">



