<?php 
require('header.php');
?>

<!-- 
<link rel="stylesheet" href="libs/jquery.qtip.min.css">

<link rel="stylesheet" href="libs/jquery-ui-1.12.1.custom/jquery-ui.theme.min.css">
<link rel="stylesheet" href="libs/jquery-ui-1.12.1.custom/jquery-ui.min.css">

<script src="libs/jquery-ui-1.12.1.custom/external/jquery/jquery.js"></script>
<script src="libs/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
<script src="libs/jquery.form.min.js"></script>
<script src="libs/jquery.qtip.min.js"></script>
<script src="js/kntloc.js"></script>

 -->

<h1>KymoKnot - A software package and webserver to identify and locate knots</h1>
<p>
  KymoKnot provides programs and libraries to localize the knotted portion 
  of knotted rings and linear chains. The Minimally-Interfering closure [1] 
  is used to circularize both linear chains and chain subportions. 
  An interactive web-server interface is available at 
  <a href="http://kymoknot.sissa.it/kymoknot/interactive.php">
     http://kymoknot.sissa.it/kymoknot/interactive.php
  </a>.
<p style="margin-bottom:0">
  If you use kymoknot in your research, please cite [2]:
</p>
<div style="padding-left:2rem">
  <i>
  Tubiana L., Polles G, Orlandini E, Micheletti C
  <a href="https://link.springer.com/article/10.1140/epje/i2018-11681-0">
    Kymoknot: A web server and software package to identify and locate knots 
    in trajectories of linear or circular polymers.
  </a> 
  The European Physical Journal E, 41(6), 72. (2018).
  </i>
</div>
<p>
  Thanks!
</p>
<p>The package currently provides 3 programs.</p>
<ul>
<li><code>KymoKnot_ring.x</code></li>
<li><code>KymoKnot_linear.x</code></li>
<li><code>K_close.x</code></li>
</ul>
<p>The first two programs locate knots, respectively on ring and linear chains, using the simplification only as a mean to reduce the number of chain subportions to be considered in the search for the knotted portion. The topology of each chain subportion is evaluated by closing the corresponding portion of the original, unsimplified chain.  <code>K_close.x</code> takes an open chain in input and closes it using the Minimally-Interfering closure scheme.</p>
<p>When searching for knotted portions, different search schemes identify different entanglement properties of a chain and may in general give different results.  For a detailed study, see ref [3].  In the current version, the bottom-up, top-down and  'unsafe' bottom-up search schemes can be used. The first two are enabled by default if no search scheme is specified by the user.</p>
<p><strong>Important.</strong> KymoKnot identifies knots based on the Alexander determinants in $t=-1$ and $t=-2$ [1]. Prime knots with 8 or more crossings can have the same Alexander determinants of other knots, including composite one; therefore the
code must be used with caution when analyzing complex knots.  The table of known knots is in the header file <code>KNT_table.h</code>.</p>
<p>[1]: 1. Tubiana L., Orlandini E, Micheletti C
<a href="http://ptp.ipap.jp/link?PTPS/191/192">Probing the Entanglement and Locating Knots in Ring Polymers: A Comparative Study of Different Arc Closure Schemes</a> Progress of Theoretical Physics supplement, 192, 192-204 (2011)</p>
<p>[2]: Tubiana L., Polles G, Orlandini E, Micheletti C 
<a href="https://link.springer.com/article/10.1140/epje/i2018-11681-0">Kymoknot: A web server and software package to identify and locate knots in trajectories of linear or circular polymers.</a> The European Physical Journal E, 41(6), 72. (2018)</p>
<p>[3]: 2. Tubiana L., Orlandini E, Micheletti C
<a href="http://prl.aps.org/pdf/PRL/v107/i18/e188302">Multiscale entanglement in ring polymers under spherical confinement</a>
Phys. Rev. Lett. 107, pg 188302 (2011).</p>
<h2>15-02-2018</h2>
<p>KymoKnot is a direct evolution of <a href="https://bitbucket.org/locknot/locknot">LocKnot</a></p>
<h2>Installation</h2>
<p>From the base directory, run <code>make</code>. This will compile also the local version of qhull needed by the Minimally-Interfering closure algorithm.</p>
<h2>Usage</h2>
<p>usage: <code>KymoKnot_ring.x [options] input_file</code> (the same holds for KymoKnot_linear.x).</p>
<p>input file must be in the format:</p>
<pre><code>    N
    x y z
    x y z
    ...
</code></pre>
<p>where N is the length of the coordinate sets.  If the input files contain coordinate of rings, the edges <code>x_0 y_0 z_0</code> and <code>x_(N-1) y_(N-1) z_(N-1)</code> must coincide.  Several configurations can be given one after the other in the same input file</p>
<h3>OUTPUT</h3>
<ul>
<li>BU__  [ -b option ]: shortest knotted portion. Bottom-up search</li>
<li>NBU__ [ -u option ]:  bottom-up search, without unknottedness check on complementar arc</li>
<li>TD__  [ -t option ]: shortest continuosly knotted portion. Top-down search</li>
</ul>
<h3>OUTPUT FILE FORMAT:</h3>
<p><code>RING i Adet_1 Adet_2 start end Length</code> where <code>i</code> is the index of the ring, <code>Adet_1</code> and <code>Adet_2</code> are the Alexander determinants start end and length are the starting point, ending point and length of the knotted portion.  In <code>KymoKnot_ring.x</code> and <code>KymoKnot_linear.x</code>, when sevearl knotted portions are found they are printed on the same line.</p>
<hr />
<h2>OPTIONS:</h2>
<ul>
<li>-h:              print this help and exit.</li>
<li>-s <max_stride>:         maximum stride for rectification. Default is none. Negative values in input set the stride to unlimited</li>
<li>-m <mem_size>:   USE WITH CAUTION! Set the size of memory manually. Insert the expected length of the chain after simplification.</li>
<li>-r <seed>:       set the pseudo random number generator's seed. Default: use time()+getpid().</li>
<li>-b:           Perform bottom-up search. ( DEFAULT )</li>
<li>-t:           Perform top-down search.  ( DEFAULT )</li>
<li>-u:           Perform bottom-up search without checking the complementar arc.</li>
<li>-F <start>: (use only in conjunction with -T) perform knot search between <start> and <end>, included.</li>
<li>-T <end>:     (use only in conjunction with -F) perform knot search between <start> and <end>, included.</li>
</ul>

<?php
include('footer.php');
?>
